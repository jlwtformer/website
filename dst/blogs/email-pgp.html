<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8"/>
	<meta content="width=device-width,initial-scale=1" name=viewport>
	<title>Encrypting Email Contents with GnuPGP &mdash; Jacob Westall</title>
 	<link rel="stylesheet" href="/css/styles.css">
    <link rel="alternate" type="application/rss+xml" title="Jacob Westall's Blog" href="https://jwestall.com/blog" />
</head>
<body>
	<header>
        <nav id=site-menu>
            <ul>
                <li><a href="/index.html">Jacob Westall</a></li>
                <li><a href="/blog.html">Blog</a></li>
                <li><a href="/design.html">Design</a></li>
                <li><a href="/privacy.html">Privacy</a></li>
                <li><a href="/cont.html">Contact</a></li>
            </ul>
        </nav>
    </header>
<main>
<h1 id="Encrypting%20Email%20Contents%20with%20GnuPGP">Encrypting Email Contents with GnuPGP</h1>

<p><em>Tuesday, June 8, 2021</em></p>

<p>Emails are of the most common and well recognized forms of digital communication. But did you know that it is also one of the least secure and least private ways to communicate as well? The protocols that would turn up to be what we know as modern email date back all the way to the early 1970&#8217;s, just about 50 years ago. While it is considered an &#8220;ancient&#8221; technology, email is still the primary method of communication for digital correspondence. So how do we take some form of technology that is bad for privacy and make it more private? We encrypt it of course!</p>

<p>Encrypting does seem like a daunting task, so it might be natural to wonder why one would go through all of the trouble to encrypt their emails. Well, in all reality not every single message will need to be encrypted, rather those that go against a user&#8217;s specific threat model. (Not sure what that is? <a href="https://ssd.eff.org/en/module/your-security-plan">read up on it here</a>.) My go-to rule of thumb is to ask myself, if this were a hand written letter I was sending through the postal mail, would I be comfortable with it going on a post card for all to see, or would I want to seal it in an envelope so only the recipient will read it? If the answer is to grab an envelope, then you should take the effort to encrypt the email.</p>

<p>The way we will encrypt is with a cool little program called GnuPG. GPG will do the heavy lifting to encrypt our email messages, so that only the sender and receiver of the messages are able to decipher the contents. Installing GPG will vary depending on your operating system. It&#8217;s heavily recommended that you do so on your desktop or laptop, as desktop or laptop computers are generally more secure than mobile phones or tablets (more on that in a future post). To see how to do so, please follow the instructions on <a href="https://gnupg.org/download/index.html">the GnuPG Website</a>.</p>

<p><em>Side Note: I will be using command line arguments when configuring my keys and encrypting&#47;decrypting messages. If you need help following along from a GUI version of GnuPG please reach out and I will do my best to help out.</em></p>

<h2 id="Generate%20your%20GPG%20Public%20and%20Private%20Keys">Generate your GPG Public and Private Keys</h2>

<p>The first thing we want to do is create our keys. The keys are cryptographic hashes that are used to verify the identity of the sender&#47;receiver so we can know for sure that the only people who can see it are you and the person you are trying to communicate. Generating the keys are simple, just run the following command:</p>

<pre><code>gpg --full-gen-key
</code></pre>

<p>Once you run that command, you will be presented with a few prompts. Here&#8217;s how I filled them out:</p>

<ul>
<li>Select RSA</li>
<li>Select 4096 so that it will make it harder to brute force our keys.</li>
<li>If you want to set an expiration date for your key you can for added security, however I set mine to never expire, so that anyone can reach out without worrying if the keys are still valid.</li>
<li>Now you will enter your name and email. This is like your public contact card, so it&#8217;s best to use the email you will be using to send your encrypted emails.</li>
<li>Comments are optional, I didn&#8217;t put one in.</li>
<li>Next it will prompt for a passphrase. It is extremely important to choose a very strong password, as this is how you will be protecting your encryption from the public and any potential attackers. A good resource on strong passwords can be found on the <a href="https://ssd.eff.org/en/module/creating-strong-passwords">Electronic Freedom Frontier website</a>.</li>
</ul>

<p>The output should look something like the following:</p>

<pre><code>gpg: &#47;home&#47;user&#47;.gnupg&#47;trustdb.gpg: trustdb created
gpg: key XXXXXXXXXXXXXXXX marked as ultimately trusted
gpg: directory &#39;&#47;home&#47;user&#47;.gnupg&#47;openpgp-revocs.d&#39; created
gpg: revocation certificate stored as &#39;&#47;home&#47;user&#47;.gnupg&#47;openpgp-revocs.d&#47;XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX.rev&#39;
public and secret key created and signed.

pub   rsa4096 YYYY-MM-DD [SC]
      XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
uid                      John Doe &#60;john.doe@example.com&#62;
sub   rsa4096 YYYY-MM-DD [E]
</code></pre>

<p>The important thing to take from this is the 40 characters listed after the &#8216;pub&#8217; label. This is your public fingerprint, which will allow others to verify that the public key they import later is the same that was just created. It&#8217;s important that you back this up, and keep it in multiple places so there is no doubt that it&#8217;s authentic.</p>

<h2 id="Export%20and%20Upload%20Public%20Key%20and%20Fingerprint">Export and Upload Public Key and Fingerprint</h2>

<p>Next step is to export your public key so you can share with others. Exporting the pub key to a file is as simple as running the following command:</p>

<pre><code>gpg --armor --export john.doe@example.net &#62; ~&#47;Desktop&#47;johndoe.asc
</code></pre>

<p>That will put a file onto your desktop that contains the public key that is safe to share with others. The best way to share that is to upload it to a public key server, like the <a href="keys.openpgp.org">OpenPGP Keyserver</a>. Only step left is to share your fingerprint (the 40 character sequence from earlier) so that others can verify the integrity of your public key file. It&#8217;s smart to place them in multiple places so that your bases are covered in case of a potential hack.</p>

<h2 id="Import%20A%20Friend&amp;#8217;s%20Public%20Key">Import A Friend&#8217;s Public Key</h2>

<p>To be able to write a message to someone that only you and them can view, you will need to import their public key. It&#8217;s pretty simple actually. Once you download their public key file, just run:</p>

<pre><code>gpg --import janedoe.asc
</code></pre>

<p>After it is imported, just run the following command to double check their fingerprint to ensure the key is authentic.</p>

<pre><code>gpg --fingerprint jane.doe@example.com
</code></pre>

<p>The output should look something like below. The 40 X characters will be their fingerprints.</p>

<pre><code>pub   rsa4096 YYYY-MM-DD [C]
      XXXX XXXX XXXX XXXX XXXX  XXXX XXXX XXXX XXXX XXXX
uid           [ unknown] Jane Doe &#60;jane.doe@example.com&#62;
sub   rsa4096 YYYY-MM-DD [S] [expires: 2021-10-25]
</code></pre>

<h2 id="Encrypt%20and%20Sign%20Messages%20for%20Sending">Encrypt and Sign Messages for Sending</h2>

<p>Writing an encrypted email is fairly simple as well. You will run another command, and the output will ask if you trust the keys of the person you are sending to, then you will type your message. Once you are finished with your message you will enter CTRL+D to finish, at which point you should be prompted to enter your encryption passphrase. The command to enter looks as follows:</p>

<pre><code>gpg --encrypt --sign --armor --output ~&#47;Desktop&#47;encrypted.asc --recipient from@example.net --recipient to@example.net
</code></pre>

<p>There&#8217;s a lot of flags on that command, so let&#8217;s take a look at what each one does for us.</p>

<ul>
<li><code>--encrypt</code> tells gpg to encrypt a message. Pretty straight forward.</li>
<li><code>--sign</code> tells gpg that we want to sign the message. Also straight forward, it&#8217;s typically used in conjunction with <code>--encrypt</code></li>
<li><code>--armor</code> tells gpg to create ASCII armored output. The default behavior is for gpg to create the binary OpenPGP format.</li>
<li><code>--output</code> is the flag that will signal that the next input is the file path where we want our encrypted message to save to. In the example above we have saved it to <code>~&#47;Desktop&#47;encrypted.asc</code></li>
<li><code>--recipient</code> will flag that the next input is who should be able to decrypt the message. It&#8217;s not necessary to put both yourself and your friend as recipients, however doing so will allow you to decrypt the message later in case you need to revisit the conversation.</li>
</ul>

<p>After this process is finished, you can attach the output file to an email to your friend. Once received, your friend can then decrypt the message to see what you have written.</p>

<h2 id="Decrypting%20a%20Received%20Message">Decrypting a Received Message</h2>

<p>Once your friend has viewed your message and has followed similar steps to encrypt a message back to you, you will be able to decrypt and view the message with the following command:</p>

<pre><code>gpg --decrypt ~&#47;Desktop&#47;encrypted.asc
</code></pre>

<p>And you will be able to see the encrypted message in your terminal output.</p>

<h2 id="Wrapping%20Things%20Up">Wrapping Things Up</h2>

<p>So that is the basic how to of encrypting an email for only one or two people to be able to see. As I mentioned before, if you were handwriting a letter, the feeling that one will need an envelope to hide the letter contents is a big indication that you should encrypt it if it is an electronic mail letter.</p>

<p>You should practice this! You can find my public key <a href="/res/jacobwestall.asc">on my website</a>, or on the <a href="https://keys.openpgp.org/search?q=jacob%40jwestall.com">OpenPGP public key server</a>. Download it, import it, and write me an encrypted message! Thanks for reading.</p>

<p><em>Note: this guide is <a href="https://gitlab.com/jlwtformer/website/-/blob/master/src/email-pgp.md">open sourced</a>, so if you find a mistake that you feel needs to be corrected, please do not hesitate to submit a change request to have it addressed. It is important that privacy and security guides such as this one are peer reviewed on a regular basis.</em></p>
</main>

<footer>
    <a href="/index.html"><img alt="" src="/res/jlw-logo.svg" height=512 width=512>Jacob Westall</a>
    <ul id=social>
        <li><a href="https://www.gitlab.com/jlwtformer">GitLab</a></li>
        <li><a href="https://twitter.com/jwestall_com">Twitter</a></li>
        <li><a rel="me" href="https://www.youtube.com/channel/UCEOuNtcigFUx8Td6es5jioA">Youtube</a></li>
        <li><a href="https://liberapay.com/Jacob_Westall/">Donate</a></li>
    </ul>
</footer>

</body>
</html>
