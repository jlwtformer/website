<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8"/>
	<meta content="width=device-width,initial-scale=1" name=viewport>
	<title>How and Why I Use macOS &mdash; Jacob Westall</title>
 	<link rel="stylesheet" href="/css/styles.css">
    <link rel="alternate" type="application/rss+xml" title="Jacob Westall's Blog" href="https://jwestall.com/blog" />
</head>
<body>
	<header>
        <nav id=site-menu>
            <ul>
                <li><a href="/index.html">Jacob Westall</a></li>
                <li><a href="/blog.html">Blog</a></li>
                <li><a href="/design.html">Design</a></li>
                <li><a href="/privacy.html">Privacy</a></li>
                <li><a href="/cont.html">Contact</a></li>
            </ul>
        </nav>
    </header>
<main>
<h1 id="How%20and%20Why%20I%20Use%20macOS">How and Why I Use macOS</h1>

<p><em>Saturday, June 19, 2021</em></p>

<p>Computers have become a ubiquitous tool for the modern age. Just like with cars, just about everyone in my corner of the world owns one, and has a history with their use. Like most families in the great American Suburbs, I grew up borrowing time on the family desktop computer, which ran the most readily available copy of Windows. I stuck with the &#8220;use what it shipped with&#8221; mentality up until I graduated high school and was gifted my own laptop. It just so happened that the preview version of Windows 8 had just released that summer, and wanting a taste of what was being touted as the &#8220;future&#8221; of desktop computing I decided to learn how to install it on my laptop.</p>

<p>That was the beginning of the end so to speak, as now being able to change the steering wheel out on my virtual car I wanted to test drive all sorts of new car steering wheels. I began searching for what else I could use to run my computer, and stumbled across an operating system called Ubuntu. Ubuntu is a popular distribution of a computer kernel called Linux, which was endlessly fascinating. I ended up staying on Windows as it was where all the tools I needed for school ran, and didn&#8217;t revisit Linux until my final year of college. From there I went a little mad with Linux, installing all sorts of OS Distributions. The thing that made me fall in love with Linux OSes was that it was completely free to use, from both a monetary and speech point of view.</p>

<p>Another reason I stuck to Linux so much was just how much I could tinker with it. Being able to dig down and tweak every part of my computer really made it feel like home. However something was always a miss with Linux, the dreaded app gap. A lot of the commercial software I became familiar with was just not available on Linux. Things such as AutoCAD, the Adobe suite of apps, the 800 LeapFrog companion apps for kid&#8217;s toys, etc. were all missing from the Linux ecosystem. While this wasn&#8217;t really an issue 75% of the time, the other 25% it was a pain trying to find comparable alternatives.</p>

<p>That is when I began messing with virtual machines to satisfy the app gap. I would run Linux on the bare metal of my computer, and then run a copy of Windows in a virtualized environment to access the apps I needed. That is when I stumbled across the <a href="https://github.cdom/foxlet/macOS-Simple-KVM">macOS simple KVM repository</a>. This repo contains tools to get a macOS virtual machine up and running in no time. Since it had been a few years since I had tried out macOS, I decided to give it an install.</p>

<p>And I must say, I have been pleasantly surprised with my experience of macOS so far. I was able to discover just how similar to Linux it was under the hood. It should be noted that macOS is not a Linux distribution, rather it has a custom kernel that is based off of BSD, which is also a UNIX based kernel similar to Linux. Having these tools and shell access that I loved about Linux, as well as being able to install the apps I needed Windows for, I admit that this once long time Apple hater fell for macOS.</p>

<p><img src="/res/setup.jpg" alt="My desktop and laptop running the VMs" /></p>

<h2 id="Setting%20Up%20a%20Useable%20macOS%20Virtual%20Machine">Setting Up a Useable macOS Virtual Machine</h2>

<p>Getting a macOS VM setup is actually pretty simple. The repository I linked to a couple paragraphs back lines out how to do so fairly simply. I&#8217;m going to walk through it here, as well as show how to change a few things to tweak it to my preferred taste.</p>

<p>First step is to install the dependencies. It should be noted that my base system, the Operating System that I am running on my computer&#8217;s hardware, is Linux (Pop!_OS to specific). The repository is setup to assume you will be running Linux as well, and while I am sure this could be done on Windows with some additional tweaking, I haven&#8217;t tried to do that so I will assume that you are running Linux as a host OS as well. Next you will need a few apps that the setup is dependent on. They are as follows (copy&#47;pasted from the repository read me):</p>

<pre><code class="language-shell">sudo apt-get install qemu-system qemu-utils python3 python3-pip  # for Ubuntu, Debian, Mint, and PopOS.
sudo pacman -S qemu python python-pip python-wheel  # for Arch.
sudo xbps-install -Su qemu python3 python3-pip   # for Void Linux.
sudo zypper in qemu-tools qemu-kvm qemu-x86 qemu-audio-pa python3-pip  # for openSUSE Tumbleweed
sudo dnf install qemu qemu-img python3 python3-pip # for Fedora
sudo emerge -a qemu python:3.4 pip # for Gentoo
</code></pre>

<p>The next step is to get the virtual system drives downloaded. There is a script in the repo that will download the setup disk for you, which can be run with <code>.&#47;jumpstart.sh</code>. This will by default download macOS Catalina recovery disk, and you can download High Sierra or Mojave with <code>.&#47;jumpstart.sh --high-sierra</code> and <code>.&#47;jumpstart.sh --mojave</code> respectively. The next virtual disk is our virtual hard drive. We&#8217;ll create it with the following command:</p>

<pre><code class="language-shell">qemu-img create -f qcow2 MyDisk.qcow2 64G
</code></pre>

<p>This command creates a 64GB virtual disk for us to use as our main hard drive for macOS. You can resize it whatever size you need by changing the <code>64G</code> parameter. Please note that it will create a file of the size specified, so make sure you have enough free space on your actual hard drive for the virtual one. Next we are going to add the following lines to the <code>basic.sh</code> script that was downloaded with the repository:</p>

<pre><code class="language-shell">    -drive id=SystemDisk,if=none,file=MyDisk.qcow2 \
    -device ide-hd,bus=sata.4,drive=SystemDisk \
</code></pre>

<p>This tells the <code>basic.sh</code> script to use the virtual hard disk we just created as the main hard drive in our macOS VM.</p>

<p>Next thing to do is to launch the virtual machine by running the <code>basic.sh</code> script! When it first boots, it will boot into the recovery disk that was pulled in by the <code>jumpstart.sh</code> script. Before we can install, we will need to launch the Disk Utility app from the menu that is presented to format our virtual hard drive for use. When the Disk Utility launches, we are going to select the disk that closest matches the size we created earlier. It won&#8217;t be the exact same size, but it&#8217;s the one that&#8217;s closest. When prompted, we&#8217;ll select the following options:</p>

<ul>
<li>Name: Whatever you want. I typically name mine <code>VirtualMac</code> or something similar.</li>
<li>Format: Select <code>APFS</code> for Apple&#8217;s Proprietary File System</li>
<li>Scheme: Stick with GUID Partition Map</li>
</ul>

<p>Then select <code>Erase</code> and let that process finish up. Once it&#8217;s done, we can now quit out of the Disk Utility and begin installing macOS to our virtual hard disk! This process will take a LONG time, and the VM will restart several times, so remember that patience is key here.</p>

<p>Once it is done, you will be greeted with the macOS setup screen. It is VERY important to NOT sign into your iCloud or AppleID when setting up the VM! Doing so could out you as using a VM to Apple, and they will ban your AppleID. I personally do not use macOS with an AppleID&#47;iCloud account, however if you are wanting to, you can follow <a href="https://github.com/foxlet/macOS-Simple-KVM/blob/master/docs/guide-Apple-ID.md">this work around to get it working without harm</a>.</p>

<p>There are some other things I do to make my configuration more enjoyable. Notably, <a href="https://github.com/foxlet/macOS-Simple-KVM/blob/master/docs/guide-performance.md">adding more RAM to the VM</a> to speed the machine up a hair, as well as <a href="https://github.com/foxlet/macOS-Simple-KVM/blob/master/docs/guide-screen-resolution.md">adjusting the VM screen resolution</a> to match that of my computer monitor. One big drawback of this setup is the graphics performance. By default, the VM will use the standard qxl video driver, which will only supply the VM with 3MB of VRAM. This is fine and all for drawing 2D objects on the display, however it is very slow at times and holds no 3D rendering capabilities. While it is possible to <a href="https://github.com/foxlet/macOS-Simple-KVM/blob/master/docs/guide-passthrough.md">pass a secondary GPU through to the VM</a>, I do not posses the hardware needed to do this.</p>

<p>This setup has allowed me to use macOS for 95% of my personal computing, which is much more than I used either Linux or Windows for respectively. With that said, I have been looking to sell some of my old computers for cheap to save up for a Macbook. While Apple&#8217;s stance on the Right to Repair movement is one that I refuse to get behind, I will excuse that in order to obtain a device that is officially supported by macOS. Having an absolute view on any computer company is not a sustainable model, and the key is to compromise when necessary.</p>

<p>If you liked reading this content please feel free to reach out and let me know. Just as well please do share with anyone you think might like it as well, as the more the audience grows for this blog the more incentive I will have to write for it. If you want to read more macOS content from me (like how to upgrade our VM to Big Sur) please reach out and let me know as well.</p>
</main>

<footer>
    <a href="/index.html"><img alt="" src="/res/jlw-logo.svg" height=512 width=512>Jacob Westall</a>
    <ul id=social>
        <li><a href="https://www.gitlab.com/jlwtformer">GitLab</a></li>
        <li><a href="https://twitter.com/jwestall_com">Twitter</a></li>
        <li><a rel="me" href="https://www.youtube.com/channel/UCEOuNtcigFUx8Td6es5jioA">Youtube</a></li>
        <li><a href="https://liberapay.com/Jacob_Westall/">Donate</a></li>
    </ul>
</footer>

</body>
</html>
