<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8"/>
	<meta content="width=device-width,initial-scale=1" name=viewport>
	<title>Learn To Program Your Own Tools: Part 2 &mdash; Jacob Westall</title>
 	<link rel="stylesheet" href="/css/styles.css">
    <link rel="alternate" type="application/rss+xml" title="Jacob Westall's Blog" href="https://jwestall.com/blog" />
</head>
<body>
	<header>
        <nav id=site-menu>
            <ul>
                <li><a href="/index.html">Jacob Westall</a></li>
                <li><a href="/blog.html">Blog</a></li>
                <li><a href="/design.html">Design</a></li>
                <li><a href="/privacy.html">Privacy</a></li>
                <li><a href="/cont.html">Contact</a></li>
            </ul>
        </nav>
    </header>
<main>
<h1 id="Learn%20To%20Program%20Your%20Own%20Tools:%20Part%202">Learn To Program Your Own Tools: Part 2</h1>

<p><em>Friday, March 18, 2022</em></p>

<p>So in <a href="/blogs/rust-lang-tutorial-p1.html">part one</a> I started laying down the basics in creating our own software tools. Now in this next part we&#8217;re gonna expand what we know to make a more useful program.</p>

<h2 id="Splitting%20Functions%20Into%20Different%20Modules">Splitting Functions Into Different Modules</h2>

<p>Before we get too deep into creating too many lines of code let&#8217;s split what we have up into different files for better organization.</p>

<p>There&#8217;s an idea in software engineering called the single responsibility principle. It pretty much means that each file should serve one overall purpose, and each function should serve one purpose, etc, etc. For something simple like what we&#8217;re doing with the small program we left off with in part one, we will only need two files. The main logic file, and the user interface file.</p>

<p>The user interface file will hold everything that relates to how the program user interacts with the program. For our program, that&#8217;s the <code>say_hello</code> and the <code>get_name</code> functions. To start, make a new file in the <code>src</code> folder in the project directory called <code>ui.rs</code>. The <code>.rs</code> file extensions tell the compiler that it&#8217;s a file containing rust code. In this file, we will be putting all of our already written code except for the <code>main</code> function.</p>

<p><em>src&#47;ui.rs</em></p>

<pre><code class="language-rust">use std::io;

pub fn say_hello(s: &#38;str) {
    println!("Hello, {}!", s);
}

pub fn get_name() -&#62; String {
    let mut name = String::new();

    println!("What is your name?");

    io::stdin().read_line(&#38;mut name).expect("Error reading from stdin.");
    
    name
}
</code></pre>

<p>Notice the new keyword <code>pub</code>, which is short for public. This lets the functions in the ui file be used outside in other files. Now we need to link it to our main file so we can use the functions in our <code>main</code> function.</p>

<p><em>src&#47;main.rs</em></p>

<pre><code class="language-rust">mod ui;

use crate::ui::*;

fn main() {
    let name = get_name();
    say_hello(name.as_str().trim());
}
</code></pre>

<p>There&#8217;s a couple new things here. <code>mod ui;</code> lets the compiler know that there is a file called <code>ui</code> that we will be using. The <code>use crate::ui::*;</code> line brings all the public functions from the <code>ui</code> module into scope of the main file. The keyword <code>crate</code> lets the compiler know to look for the <code>ui</code> library in the project directory. The last new thing is the <code>.trim()</code> call on the <code>name</code> variable. This takes all the leading or trailing white space off of the <code>name</code> variable, fixing our issue of the <code>!</code> being on its own line in the output.</p>

<p><img src="/res/rust-lang-tut2-output-1.png" alt="The output of all of our work" /></p>

<h2 id="Making%20Something%20More%20Useful">Making Something More Useful</h2>

<p>Now that we have some of the basics under our belt, let&#8217;s start coding something more useful. A unit conversion tool! Now there are many different types of units, but we&#8217;ll go with the only one that is consistent across the entire planet, time. We&#8217;ll convert between Days, Hours, Minutes, and Seconds.</p>

<p>To start, we&#8217;re going to have to rework our <code>ui.rs</code> file. Update it to match what is below.</p>

<p><em>src&#47;ui.rs</em></p>

<pre><code class="language-rust">use std::io;

pub fn get_units(s: &#38;str) -&#62; u8 {
    println!("Please select the {} units:", s);

    let mut input = String::new();
    io::stdin().read_line(&#38;mut input).expect("Error reading from stdin.");
    let input: u8 = input.trim().parse().expect("Error converting String to u8.");

    input
}

pub fn get_value(s: &#38;str) -&#62; f32 {
    println!("Please enter the {} value:", s);

    let mut input = String::new();
    io::stdin().read_line(&#38;mut input).expect("Error reading from stdin.");
    let input: f32 = input.trim().parse().expect("Error converting String to f32.");

    input
}
</code></pre>

<p>There isn&#8217;t much here that we don&#8217;t know already, save for a piece of one line. <code>let input: f32 = input.trim().parse().expect("Error converting String to f32.");</code> The new part here is the <code>.parse()</code> command. This is a command that will convert a variable from one type to another. This is one of the instances where we want a specific type of variable, so with <code>let input: f32</code> we are saying that we will reassign the input value, and it needs to be a variable of type <code>f32</code>. The <code>f32</code> type is a number with a &#8216;floating&#8217; decimal value. The <code>u8</code> type in this file represents an unsigned 8-bit integer number. For those who don&#8217;t understand, it&#8217;s a positive only whole number that can be stored in 8-bits of memory. If we were to want to keep track of negative and positive numbers, we would use the <code>i8</code> type. We don&#8217;t need to here, so we&#8217;re sticking with <code>u8</code>.</p>

<p>Next we&#8217;re going to be adding another file to our project, one to hold all of our auxiliary functions that aren&#8217;t related to the user interface. We could put these in <code>main.rs</code>, however I like to keep only the <code>main</code> function in the main file to keep things tidy. We&#8217;ll make a file called <code>utils.rs</code> in the <code>src</code> folder with the following contents.</p>

<p><em>src&#47;utils.rs</em></p>

<pre><code class="language-rust">pub fn to_minutes(input: f32, units: u8) -&#62; f32 {
    match units {
        1 =&#62; input * 24 as f32 * 60 as f32,
        2 =&#62; input * 60 as f32,
        3 =&#62; input,
        4 =&#62; input &#47; 60 as f32,
        _ =&#62; panic!("Unable to convert units. Invalid option supplied.")
    }
}

pub fn from_minutes(input: f32, units: u8) -&#62; f32 {
    match units {
        1 =&#62; input &#47; 24 as f32 &#47; 60 as f32,
        2 =&#62; input &#47; 60 as f32,
        3 =&#62; input,
        4 =&#62; input * 60 as f32,
        _ =&#62; panic!("Unable to convert units. Invalid option supplied.")
    }
}
</code></pre>

<p>So these two functions make use of a <code>match</code> keyword that we haven&#8217;t used yet. This is a pattern matching tool. Essentially it evaluates the value of <code>units</code> and returns a different function per each value. So, when we <code>match</code> the <code>units</code> variable, if it is of value <code>2</code> we will return the result of <code>input * 60 as f32</code>. The <code>as f32</code> keywords make it so that the <code>60</code> value is evaluated as a floating point type, instead of an integer type. We do this because <code>input</code> is required to be <code>f32</code> type per the function declaration. The compiler can&#8217;t multiply or divide a floating point number by an integer as they aren&#8217;t the same type, so we need to specify that our math is to be done in floating point.</p>

<p>Now all we have to do is update our <code>main.rs</code> file to finish it off.</p>

<pre><code class="language-rust">mod ui;
mod utils;

use crate::ui::*;
use crate::utils::*;

fn main() {
    println!("Units:\n  1. Days\n  2. Hours\n  3. Minutes\n  4. Seconds");

    let in_units = get_units("starting");
    let out_units = get_units("final");
    let input = get_value("starting");
    let output = from_minutes(to_minutes(input, in_units), out_units);

    println!("The final value is: {}", output);
}
</code></pre>

<p>Pretty straight forward. The <code>\n</code> characters in the first <code>println!</code> call is what is referred to as a NewLine character. It will put all text after it on a new line. The rest is all the calls to the functions we have written so far.</p>

<p>Now, when we run our program it should look like the below:</p>

<p><img src="/res/rust-lang-tut2-output-2.png" alt="The output of the final program" /></p>

<p>So here we are, we have finally put we know together to make a useful command line program. Now while I and many others are fond of command lines, the modern era prefers a more graphical approach. That will take much more work, so we&#8217;ll save it for another time.</p>

<p>Let me know what you think, do you want to see a part 3? I&#8217;ll be more than happy to hear whatever feedback you have.</p>

<p>Jacob</p>
</main>

<footer>
    <a href="/index.html"><img alt="" src="/res/jlw-logo.svg" height=512 width=512>Jacob Westall</a>
    <ul id=social>
        <li><a href="https://www.gitlab.com/jlwtformer">GitLab</a></li>
        <li><a href="https://twitter.com/jwestall_com">Twitter</a></li>
        <li><a rel="me" href="https://www.youtube.com/channel/UCEOuNtcigFUx8Td6es5jioA">Youtube</a></li>
        <li><a href="https://liberapay.com/Jacob_Westall/">Donate</a></li>
    </ul>
</footer>

</body>
</html>
