<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8"/>
	<meta content="width=device-width,initial-scale=1" name=viewport>
	<title>Creating an RSS Feed for a Static Website &mdash; Jacob Westall</title>
 	<link rel="stylesheet" href="/css/styles.css">
    <link rel="alternate" type="application/rss+xml" title="Jacob Westall's Blog" href="https://jwestall.com/blog" />
</head>
<body>
	<header>
        <nav id=site-menu>
            <ul>
                <li><a href="/index.html">Jacob Westall</a></li>
                <li><a href="/blog.html">Blog</a></li>
                <li><a href="/design.html">Design</a></li>
                <li><a href="/privacy.html">Privacy</a></li>
                <li><a href="/cont.html">Contact</a></li>
            </ul>
        </nav>
    </header>
<main>
<h1 id="Creating%20an%20RSS%20Feed%20for%20a%20Static%20Website">Creating an RSS Feed for a Static Website</h1>

<p><em>Friday, July 2, 2021</em></p>

<p>One of the ways a content creator can share their blog posts or YouTube videos is with an RSS feed. RSS stands for Really Simple Syndication, which is an apt name for what the service provides. Simply put, a content creator will provide an RSS feed document, and a reader will link that feed document to their preferred RSS reader, and the reader will scan for changes to the document and will notify the reader when there is new content from the creator.</p>

<p>Typically RSS feeds are live documents hosted on a website, that will update automatically when new content is added. As I explained in my post on making a simple website, some web hosting solutions (like Git Pages) will not allow for dynamic content. So, to have an RSS feed, the creator of the static site would need to update the RSS document manually (or automate it behind the scenes). Creating one is actually pretty simple, and I can show you how I made one for this blog with my own.</p>

<h2 id="Creating%20an%20RSS%20Feed%20Document">Creating an RSS Feed Document</h2>

<p>Creating one is about as easy as making a static webpage. The RSS document is just an XML file, often called <code>feed.xml</code> that is hosted on your platform. To start, the first line of the XML file will need to have some information about the document itself, and will look something like below:</p>

<pre><code class="language-XML">&#60;?xml version="1.0" encoding="utf-8"?&#62;
</code></pre>

<p>The two properties being declared here are self explanatory. A <code>version</code> tag to let the reader know what version of XML document it is, and an <code>encoding</code> declaration to let the reader know what character encoding the document is written in.</p>

<p>Next we will open up an <code>rss</code> tag, which will contain RSS specific properties, and will wrap all of the content of the rest of the RSS Feed document.</p>

<pre><code class="language-XML">&#60;rss version="2.0" xmlns:atom="http:&#47;&#47;www.w3.org&#47;2005&#47;Atom"&#62;
&#60;&#47;rss&#62;
</code></pre>

<p>This tag has a few similar properties:</p>

<ul>
<li><code>version</code> Declares which RSS version the document is.</li>
<li><code>xmlns:atom</code> Declares a reference namespace. This is that we can use custom XML tags from the document it links to.</li>
</ul>

<p>Next we will put a <code>channel</code> tag inside the <code>rss</code> tag. Inside the <code>channel</code> tag we will put more tags that will give information pertaining to our website, as seen below:</p>

<pre><code class="language-XML">&#60;channel&#62;                  
    &#60;title&#62;Jacob Westall&#39;s Blog&#60;&#47;title&#62;
    &#60;description&#62;A blog full of junk.&#60;&#47;description&#62;
    &#60;link&#62;https:&#47;&#47;jwestall.com&#47;blog&#60;&#47;link&#62;
    &#60;atom:link href="https:&#47;&#47;jwestall.com&#47;feed.xml" rel="self" type="application&#47;rss+xml"&#47;&#62;
&#60;&#47;channel&#62;
</code></pre>

<p>The tags within <code>channel</code> are self explanatory, with exception of the last. The <code>atom:link</code> tag is one that holds RSS specific properties, and is using the <code>link</code> tag that is found at the link in our namespace declaration in the <code>rss</code> tag.</p>

<p>From here on we will place similar tag groups within the <code>channel</code> tag that will contain information about each of our blog&#47;content posts.</p>

<pre><code class="language-XML">&#60;item&#62;             
    &#60;title&#62;How and Why I Run This Website&#60;&#47;title&#62;
    &#60;link&#62;https:&#47;&#47;jwestall.com&#47;how-why-website.html&#60;&#47;link&#62;
    &#60;guid&#62;https:&#47;&#47;jwestall.com&#47;how-why-website.html&#60;&#47;guid&#62;
    &#60;pubDate&#62;Wed, 30 Jun 2021 00:00:00 CST&#60;&#47;pubDate&#62; 
&#60;&#47;item&#62;
</code></pre>

<p>Some things to note are the <code>guid</code> tag and the <code>pubDate</code> tag. A GUID is a unique identifier that will differentiate the <code>item</code> group from other similar <code>item</code> groups. It can be just about anything, but I use the blog post link to keep things simple. The <code>pubDate</code> tag is also important, as not just any date can be thrown in. It requires that you use the <a href="https://datatracker.ietf.org/doc/html/rfc822#section-5">RFC-822 date-time format</a>. It&#8217;s not that hard of a standard to conform to, but an important one none the less.</p>

<p>So with all of our components put together, we get the following XML document:</p>

<pre><code class="language-XML">&#60;?xml version="1.0" encoding="utf-8"?&#62;
&#60;rss version="2.0" xmlns:atom="http:&#47;&#47;www.w3.org&#47;2005&#47;Atom"&#62;
&#60;channel&#62;                  
    &#60;title&#62;Jacob Westall&#39;s Blog&#60;&#47;title&#62;
    &#60;description&#62;A blog full of junk.&#60;&#47;description&#62;
    &#60;link&#62;https:&#47;&#47;jwestall.com&#47;blog&#60;&#47;link&#62;
    &#60;atom:link href="https:&#47;&#47;jwestall.com&#47;feed.xml" rel="self" type="application&#47;rss+xml"&#47;&#62;

    &#60;item&#62;             
        &#60;title&#62;Creating an RSS Feed for a Static Website&#60;&#47;title&#62;
        &#60;link&#62;https:&#47;&#47;jwestall.com&#47;making-rss-feed.html&#60;&#47;link&#62;
        &#60;guid&#62;https:&#47;&#47;jwestall.com&#47;making-rss-feed.html&#60;&#47;guid&#62;
        &#60;pubDate&#62;Fri, 2 Jul 2021 00:00:00 CST&#60;&#47;pubDate&#62; 
    &#60;&#47;item&#62;

    &#60;item&#62;             
        &#60;title&#62;How and Why I Run This Website&#60;&#47;title&#62;
        &#60;link&#62;https:&#47;&#47;jwestall.com&#47;how-why-website.html&#60;&#47;link&#62;
        &#60;guid&#62;https:&#47;&#47;jwestall.com&#47;how-why-website.html&#60;&#47;guid&#62;
        &#60;pubDate&#62;Wed, 30 Jun 2021 00:00:00 CST&#60;&#47;pubDate&#62; 
    &#60;&#47;item&#62;

&#60;&#47;channel&#62;
&#60;&#47;rss&#62;
</code></pre>

<p>A good way to make sure your RSS Feed document is properly formatted, is to run it through the <a href="https://validator.w3.org/feed/">W3C Validation Tool</a>. This site will analyze your feed document and will let you know what changes will need to be made to make sure it&#8217;s a valid document.</p>

<h2 id="Adding%20an%20RSS%20Feed%20to%20a%20Reader">Adding an RSS Feed to a Reader</h2>

<p>Different readers will have different methods of adding feeds, however most will be as simple as linking the feed document. Simple as that.</p>

<p>If you want to subscribe to my blog, you can find my RSS Feed Doc at <code>https:&#47;&#47;jwestall.com&#47;feed.xml</code>. That way, you won&#8217;t have to miss any blog posts from yours truly.</p>
</main>

<footer>
    <a href="/index.html"><img alt="" src="/res/jlw-logo.svg" height=512 width=512>Jacob Westall</a>
    <ul id=social>
        <li><a href="https://www.gitlab.com/jlwtformer">GitLab</a></li>
        <li><a href="https://twitter.com/jwestall_com">Twitter</a></li>
        <li><a rel="me" href="https://www.youtube.com/channel/UCEOuNtcigFUx8Td6es5jioA">Youtube</a></li>
        <li><a href="https://liberapay.com/Jacob_Westall/">Donate</a></li>
    </ul>
</footer>

</body>
</html>
