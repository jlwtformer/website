<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8"/>
	<meta content="width=device-width,initial-scale=1" name=viewport>
	<title>Learn To Program Your Own Tools: Part 1 &mdash; Jacob Westall</title>
 	<link rel="stylesheet" href="/css/styles.css">
    <link rel="alternate" type="application/rss+xml" title="Jacob Westall's Blog" href="https://jwestall.com/blog" />
</head>
<body>
	<header>
        <nav id=site-menu>
            <ul>
                <li><a href="/index.html">Jacob Westall</a></li>
                <li><a href="/blog.html">Blog</a></li>
                <li><a href="/design.html">Design</a></li>
                <li><a href="/privacy.html">Privacy</a></li>
                <li><a href="/cont.html">Contact</a></li>
            </ul>
        </nav>
    </header>
<main>
<h1 id="Learn%20To%20Program%20Your%20Own%20Tools:%20Part%201">Learn To Program Your Own Tools: Part 1</h1>

<p><em>Thursday, February 17, 2022</em></p>

<p>While the main focus of my blog is to provide insights into things that I find interesting, I hope that one thing is clear with my works. That you have the ability to do just about anything yourself. And that holds true for just about anything, from writing to video production to even coding.</p>

<p>And that is what we will be doing today, exploring how any one can get up and running easily with coding. What we&#8217;re gonna do today is get set up with the Rust programming language, and build a small GUI tool to convert unit measurements. One of the reasons I think the average Joe should learn how to code is because it&#8217;s the tools of our modern times. Programming languages are to today like what hammers and drills were during the industrial revolution. It will do nothing but benefit someone to learn how to code, so let&#8217;s get started!</p>

<h2 id="Gathering%20Our%20Tools">Gathering Our Tools</h2>

<p>Just like with simple tools we use in an office setting, a stapler, a chair, etc, we need to have a means to build our tools. Staplers don&#8217;t grow on trees, and neither do programs. We&#8217;re going to be using two main tools in order to build our program: a compiler and an Integrated Development Environment (IDE).</p>

<p>The compiler will take the code we&#8217;ve written and translate it to a machine code executable, enabling us to run our program. The IDE is a special kind of text editor that highlights different code syntax, provides insight into possible errors, and in some cases contains a compiler.</p>

<p>The compiler we will use is cargo, which is a package manager for the rust programming language. Now you might ask why we are going to utilize rust as opposed to other programming languages like Python or C&#47;C++. One thing I have come to like about rust is that it is written in a high level way like python, but still provides the low level control that can be found in C&#47;C++. That, and it is very memory safe, and will do it&#8217;s best to prevent you from shooting yourself in the foot when programming.</p>

<p>Now, to install cargo and the rust tool chain all you have to do is go to the Rust Lang Website, and go to the <a href="https://www.rust-lang.org/learn/get-started">Getting Started</a> page. From there it will give you a command to run on your computer&#8217;s command prompt depending on what OS you are running. There are also other <a href="https://forge.rust-lang.org/infra/other-installation-methods.html">methods to install the tool chain</a> if you aren&#8217;t yet comfortable with the command line.</p>

<p>Next let&#8217;s grab our IDE. The most commonly used one is Microsoft&#8217;s own <a href="https://code.visualstudio.com/">Visual Studio Code</a>. It is fully cross platform so it will work on any desktop or laptop computer. The install instructions will vary depending on your OS, but it should be as straight forward as installing any other piece of software.</p>

<h2 id="Setting%20Up%20The%20Tools">Setting Up The Tools</h2>

<p>Once you have cargo and VS Code installed, we need a way for them to work together to give us the most benefit. In VS Code, you&#8217;ll want to open the extensions. That&#8217;s the icon on the left sidebar with 4 squares. In the text box that says <code>Search Extensions in Marketplace</code>, we will type in rust. The top result should be an extension called <code>Rust</code> by rust-lang. Go ahead and enable this extension (see screenshot below if you aren&#8217;t sure).</p>

<p><img src="/res/rust-lang-tut-extension-page.png" alt="Rust in the VS Code Extensions Store" /></p>

<p>Next we will need to set up our development folder. Thankfully cargo makes this very easy. In VS Code, press the <code>CTRL</code> and the <code>~</code> key to open up the command prompt window. By default the command prompt will drop us in our &#8220;Home&#8221; folder, or where our Documents, Pictures, etc, folders reside. We are going to move to the desktop by entering <code>cd Desktop</code> into the command prompt.</p>

<p>Now, we will create the project folder. On the command prompt, type in <code>cargo new unit-conv</code>. This will create a folder that holds all of our project files.</p>

<p><img src="/res/rust-lang-tut-new-project.png" alt="Creating a new project in VS Code" /></p>

<p>Now we will press <code>CTRL</code> and <code>K</code>, and then <code>CTRL</code> and <code>O</code> to bring up the open folder dialog. Navigate to the project folder we just created, then press open. VS Code will ask to download additional components for the rust compiler extension, allow it to do so. After that we are all set up and can begin coding!</p>

<h2 id="Coding%20The%20First%20Project">Coding The First Project</h2>

<p>Now we are ready to code, almost. You&#8217;ll notice some files and folders already in the project folder when we open it in VS Code. There will be a folder called <code>src</code>, that is where the source code of our project will all be held. Next is a file called <code>Cargo.toml</code>. This is a text file that will hold all the project information. We&#8217;ll touch on that in a little bit. Next is a file called <code>.gitignore</code>. This one isn&#8217;t important to us today, but it is a file that lists all the folders&#47;files that we want our source control to ignore when uploading to a git repository. We won&#8217;t cover that today so we can ignore it for now.</p>

<p>Let&#8217;s open up the file <code>src&#47;main.rs</code>. It should just have three lines of code, just like below.</p>

<pre><code class="language-rust">fn main() {
    println!("Hello, world!");
}
</code></pre>

<p>The first line is a function declaration. Functions are ways to group lines of code together, so we don&#8217;t have to write them over and over again. This specific function is the <code>main</code> function. without a main function, the program will not know where to begin executing our lines of code and will fail to compile. The empty parenthesis <code>()</code> tell the compiler that no arguments will be needed to run the program. There are situations where we will put things in here, but right now we won&#8217;t worry about it. The open bracket <code>{</code> lets the compiler know the following lines of code are all a part of the main function.</p>

<p>On the next line we have <code>println!("Hello, world!");</code>. This is an example of how we would call a function. The function being called is <code>println!</code> and the argument given is <code>"Hello, world!"</code>. We will end every line of code execution with a semicolon, <code>;</code>. There are some exceptions to this rule which we will get to later.</p>

<p>The last line in this example program is a closing bracket, <code>}</code>. It lets the compiler know that the main function is complete.</p>

<p>Now we will run this program. Press the <code>CTRL</code> and <code>~</code> keys to bring up the command prompt. It will bring it up in the project directory, so need to <code>cd</code> anywhere else. Type in the command <code>cargo run</code> to build (compile an executable) and run the program. The output should look something like below.</p>

<p><img src="/res/rust-lang-tut-output-1.png" alt="Hello, world! output" /></p>

<p>As you can see, all the program did was print the text <code>Hello, world!</code> to the console output. The function <code>println!</code> is one that is built into rust, and available to use right out of the box. But how do we make our own functions? Well that part is pretty simple.</p>

<h2 id="Creating%20Custom%20Functions">Creating Custom Functions</h2>

<p>So when we looked at how a basic function like <code>main</code> was set up, we saw that it did not take in any arguments. Now let&#8217;s make a function that does. The code below is an example of a basic function that takes in one argument.</p>

<pre><code class="language-rust">fn main() {
    say_hello("Jacob");
}

fn say_hello(s: &#38;str) {
    println!("Hello, {}!", s);
}
</code></pre>

<p>As you can see we made a new function named <code>say_hello</code> that takes in one argument. Arguments can be thought of as inputs that can and will change depending on how they are used. Another name for these arguments are variables. Variables in programming terms are pieces of information that can vary depending on how they are used. In our function, we defined a variable <code>s</code> as an argument for our function. The <code>: &#38;str</code> bit just tells the compiler what type of variable we will need, whether it be an integer, a decimal number, a text character, or a string of characters. <code>&#38;str</code> refers to a pointer to a text string variable. A pointer is literally pointing to the memory slot on the computer RAM that the value is held in. Because our text input is unknown to us while making the function, we use a pointer to keep it a little open ended.</p>

<p>Next we have that <code>println!</code> command again, this time with two arguments that are separated by a comma. The text string, <code>Hello, {}!</code> and our argument variable <code>s</code>. When we compile this program, the output will be <code>Hello, (insert value of s here)!</code>. Because we use that function in main with the argument <code>"Jacob"</code>, the output looks like the following:</p>

<p><img src="/res/rust-lang-tut-output-2.png" alt="Hello, Jacob! output" /></p>

<p>Now let&#8217;s make another function, this time one that returns a value. Let&#8217;s add on to our program:</p>

<pre><code class="language-rust">use std::io;

fn main() {
    let name = get_name();
    say_hello(name.as_str());
}

fn say_hello(s: &#38;str) {
    println!("Hello, {}!", s);
}

fn get_name() -&#62; String {
    let mut name = String::new();

    println!("What is your name?");

    io::stdin().read_line(&#38;mut name).expect("Error reading from stdin.");
    
    name
}
</code></pre>

<p>Now there&#8217;s a lot to unpack here, so let&#8217;s start from the first line. <code>use std::io;</code>. This line is bringing a precompiled code library into scope for us to use. A software library is a set of functions that have already been written that we can use to make our lives easier. Think of it as a tool box, we wouldn&#8217;t build a new hammer every time we needed one on a project, so we get one we already have from a tool box to use.</p>

<p>Now let&#8217;s look at the declaration of our new function. <code>fn get_name() -&#62; String</code>. This is a typical function call out, with something new. The <code>-&#62; String</code> portion of this declaration tells the compiler that when the function is called, a variable of type <code>String</code> will be returned. So in the top of our program, where we have the line <code>let name = get_name();</code> we just created the variable <code>name</code> with the value that is returned from the <code>get_name</code> function.</p>

<p>Now you probably noted that we have both <code>&#38;str</code> and now <code>String</code> variable types. One is a pointer to the value&#8217;s location in the RAM memory, and the other is the data itself. That&#8217;s my brief understanding, hopefully someone much more knowledgeable will let me know if this is correct or not and I will update accordingly.</p>

<p>So, in the <code>get_name</code> function we will do a few things. We define a variable <code>name</code> for the scope of the <code>get_name</code> function, one that is separate from the <code>name</code> variable in the scope of the <code>main</code> function. Note that we put a keyword, <code>mut</code> after our <code>let</code> in the <code>get_name</code> function. That is to make this variable mutable, which lets us change the value of that variable without having to re assign it outright. In the <code>main</code> function, we leave <code>name</code> as immutable, since once it&#8217;s defined we won&#8217;t be changing it. In <code>get_name</code> we assign the variable <code>name</code> as <code>String::new()</code>. This tells the compiler we want an empty variable of type <code>String</code>. We will fill in the variable later.</p>

<p>Next we will be printing a query to the console output, and then we will have this line: <code>io::stdin().read_line(&#38;mut name).expect("Error reading from stdin.");</code>. Now there&#8217;s a lot to this line so we&#8217;ll go through it piece by piece. <code>io::stdin()</code> is a tool from the <code>std::io</code> library we imported at the top of our program. The <code>.read_line(&#38;mut name)</code> piece is the tool from <code>stdin()</code> that reads what the user types, and adds it to the end of the <code>name</code> variable string. The <code>.expect("Error reading from stdin.");</code> piece tells the compiler that in the event the computer can&#8217;t read the input from the console, error out and spit out the string of text <code>Error reading from stdin.</code> You&#8217;ll notice we need this for the program to not error out on compile, this is because the <code>read_line()</code> function returns either an <code>Ok</code> or <code>Err</code> value. They <code>expect()</code> piece handles this return.</p>

<p>Now we have the last line of our function, <code>name</code>. Notice that we do not have the semicolon on this line, this is because we are telling the compiler to return the value of the variable <code>name</code> for when the function is called.</p>

<p>When our entire program is compiled and run, the output should look like the below:</p>

<p><img src="/res/rust-lang-tut-output-3.png" alt="Hello, Jacob ! output" /></p>

<p>Note that the <code>!</code> is on a separate line, that is because the <code>read_line</code> function captures when we press the enter key and adds it to the <code>name</code> variable. There&#8217;s ways to fix this, but we won&#8217;t worry too much about it for now.</p>

<h2 id="Next%20Steps">Next Steps</h2>

<p>So this blog post is getting a little long, so I&#8217;ll cut it off here for now. Did you like taking the time to dive into rust? Do you want to see a part 2 or part 3 where we make a simple unit converter with a fully usable graphic user interface? Let me know and I&#8217;ll be more than happy to keep this project going.</p>

<p>Thanks,
Jacob</p>
</main>

<footer>
    <a href="/index.html"><img alt="" src="/res/jlw-logo.svg" height=512 width=512>Jacob Westall</a>
    <ul id=social>
        <li><a href="https://www.gitlab.com/jlwtformer">GitLab</a></li>
        <li><a href="https://twitter.com/jwestall_com">Twitter</a></li>
        <li><a rel="me" href="https://www.youtube.com/channel/UCEOuNtcigFUx8Td6es5jioA">Youtube</a></li>
        <li><a href="https://liberapay.com/Jacob_Westall/">Donate</a></li>
    </ul>
</footer>

</body>
</html>
