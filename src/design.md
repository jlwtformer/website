# 3D Design And Printing Services

From concept, to design, to manufacturing; 3D printing has revolutionized the at home manufacturing market. While it is easier than ever to get started printing at home, some might not have the resources to create their own concepts or products at home.

For that reason, I am offering my knowledge and expertise in this field to help enthusiasts and small businesses get their ideas off the ground. With a decade in CAD Design and 3D Modeling, and over half a decade in 3D printing, I would be more than happy to supply my knowledge and expertise to help you bring your ideas to life.

Whether you have just an idea, or a model you just want printed, please reach out and we can work out how to best realize your vision.

_From Concept..._

![Concept](/res/concept_picture.png)

_To Design..._

![Design](/res/design_picture.png)

_To Manufacturing..._

![Manufactured](/res/manufactured_picture.png)
