# Blog

This is the place where I share interesting things about myself and the hobbies I find myself diving into. If you want to add my blog to your RSS feed, the link to do so is at `https://jwestall.com/feed.xml`.

I am also currently keeping up with a vlog, which can be found on [YouTube](https://www.youtube.com/channel/UCEOuNtcigFUx8Td6es5jioA) and [TilVids](https://tilvids.com/c/jacob_westall_channel/videos).

## June 2022

[A Personal Update](blogs/personal-update-1.html)

## May 2022

[Making A Tracker For My Projects](blogs/making-project-tracker.html)

## April 2022

[Fedora 36 Beta - Seriously Tempting](blogs/fedora-36-beta.html)

## March 2022

[Losing An Idol](blogs/losing-an-idol.html)

[The Things I Like About Manjaro, And The Thing I Don't](blogs/manjaro-review-early-2022.html)

[Learn To Program Your Own Tools: Part 2](blogs/rust-lang-tutorial-p2.html)

## February 2022

[Learn To Program Your Own Tools: Part 1](blogs/rust-lang-tutorial-p1.html)

## January 2022

[Making a DIY Boba Fett Armor: Part 1](blogs/diy-boba-fett-p1.html)

[A New Perspective](blogs/a-new-perspective.html)

## December 2021

[Say Hello To helloSystem](/blogs/hello-system-review.html)

## November 2021

[My Thoughts On The Controversy Between Linus Sebastian and Pop!_OS](blogs/linus-pop-thoughts.html)

[The First Step Into 3D Printing, Know Your Printer](/blogs/3d-printer-first-steps.html)

## October 2021

[Starting Video Logs](/blogs/starting-video-log.html)

[Making A Program To Pick Blog Topics For Me](/blogs/making-topic-picker.html)

[Magical Series Part Two: Setting A Foundation And The First Line of Code](/blogs/magical-series-p2.html)

[The Linux Distribution You Should Switch To](/blogs/switch-to-linux.html)

## September 2021

[3D Printing Prototypes: Spider-Man Armor](/blogs/prototype-spider-man-add-on.html)

[My History With Social Media, Why I Left, And Why I Am Returning](/blogs/social-media-history.html)

[Creating a Chat Room For Readers of This Blog](blogs/official-element-chat.html)

## August 2021

[Reset.](blogs/reset.html)

## July 2021

[Privacy Isn't About What You Have To Hide, It's About What You Have To Lose](blogs/privacy-matters.html)

[From macOS to Windows 10](blogs/from-mac-to-pc.html)

[No, Your Computer Isn't Yours. But That Can Be Okay](blogs/computer-isnt-yours.html)

[Let's Make a Video Game - The Magical Series Introduction](blogs/magical-series-intro.html)

[3D Printing is Freaking Awesome](blogs/3D-printing-awesome.html)

[Creating an RSS Feed for a Static Website](blogs/making-rss-feed.html)

## June 2021

[How and Why I Run This Website](blogs/how-why-website.html)

[How and Why I Use macOS](blogs/how-why-macos.html)

[A Wild Donation Link Appears!](blogs/wild-donation-link.html)

[Fan Fiction: Star Wars Echoes of the Beyond - Chapter 2](blogs/sweob-c2.html)

[Fan Fiction: Star Wars Echoes of the Beyond - Chapter 1](blogs/sweob-ch1.html)

[Encrypting Email Contents with GnuPGP](blogs/email-pgp.html)

[Sloth and Smell the Roses](blogs/sloth-smell-roses.html)
