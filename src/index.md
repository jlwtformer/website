# Welcome

My name is Jacob Westall, welcome to my website. This site is where I keep information on my professional background, as well as my personal blog where I share interesting topics and tutorials. Feel free to browse as you please, and stop by the contact page to leave some feedback.

## About

I'm an engineer by day and father by evening. I find myself getting wrapped up in multitudes of hobbies, all of which you can read about in my [blog](blog.html).