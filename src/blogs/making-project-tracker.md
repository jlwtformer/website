# Making A Tracker For My Projects

One of the things I am not the best at is organizing my personal projects. It's something I wanted to improve on over the year, and looking into one of the YouTube channels I follow gave me the best way to organize. Making a project tracker that's open to you to watch as well!

The YouTube channel I mentioned is Internet Historian. Yup, good ole Historian. I found his Twitter, and in turn found out he has a public Trello board where he provides updates for the videos he's working on. I liked this, and decided to steal the idea, kinda. I won't be using a Trello board, not because I don't like the company/service or whatever. I wanted to keep my tracker on my site, and keep it private for those who want or need to avoid services and sites like Trello.

So, where is it? Well it's right here on my website, [right here](/blogs/project-tracker.html). I'll have a link to it pinned on my Mastodon and Twitter profile, so it'll be easy to find if anyone wants to look for it.

Thanks, Jacob
