# Losing An Idol

_Saturday, March 26, 2022_

On January 7, 2020, former drummer of Rush, Neil Peart passed away. On July 26, 2021, former drummer of Slipknot, Joey Jordinson passed away. On March 25, 2022, Taylor Hawkins, drummer for the Foo Fighters passed away. In the last three years I have lost three of my drumming influences and idols. It's often that one gets lost in the ebb and flow of the day to day and forgets just how real human mortality truly is. We forget that the ones we hold dear aren't going to be around forever. It's times like this that I get an unwelcome reminder of just how important the ones I hold dear are to me.

While their styles are varied, each of them had a distinct impact on not just myself but the world around me as well. Life will move on, and their memories will carry on for longer than I will be around. I may have never met any of them personally, but did get the pleasure of seeing one of them perform live. Thanks to them I have been able to find motivation to keep on every day, and thanks to them so many others will too.

Jacob
