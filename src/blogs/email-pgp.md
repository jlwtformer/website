# Encrypting Email Contents with GnuPGP

_Tuesday, June 8, 2021_

Emails are of the most common and well recognized forms of digital communication. But did you know that it is also one of the least secure and least private ways to communicate as well? The protocols that would turn up to be what we know as modern email date back all the way to the early 1970's, just about 50 years ago. While it is considered an "ancient" technology, email is still the primary method of communication for digital correspondence. So how do we take some form of technology that is bad for privacy and make it more private? We encrypt it of course!

Encrypting does seem like a daunting task, so it might be natural to wonder why one would go through all of the trouble to encrypt their emails. Well, in all reality not every single message will need to be encrypted, rather those that go against a user's specific threat model. (Not sure what that is? [read up on it here](https://ssd.eff.org/en/module/your-security-plan).) My go-to rule of thumb is to ask myself, if this were a hand written letter I was sending through the postal mail, would I be comfortable with it going on a post card for all to see, or would I want to seal it in an envelope so only the recipient will read it? If the answer is to grab an envelope, then you should take the effort to encrypt the email.

The way we will encrypt is with a cool little program called GnuPG. GPG will do the heavy lifting to encrypt our email messages, so that only the sender and receiver of the messages are able to decipher the contents. Installing GPG will vary depending on your operating system. It's heavily recommended that you do so on your desktop or laptop, as desktop or laptop computers are generally more secure than mobile phones or tablets (more on that in a future post). To see how to do so, please follow the instructions on [the GnuPG Website](https://gnupg.org/download/index.html).

_Side Note: I will be using command line arguments when configuring my keys and encrypting/decrypting messages. If you need help following along from a GUI version of GnuPG please reach out and I will do my best to help out._

## Generate your GPG Public and Private Keys

The first thing we want to do is create our keys. The keys are cryptographic hashes that are used to verify the identity of the sender/receiver so we can know for sure that the only people who can see it are you and the person you are trying to communicate. Generating the keys are simple, just run the following command:

```
gpg --full-gen-key
```

Once you run that command, you will be presented with a few prompts. Here's how I filled them out:

* Select RSA
* Select 4096 so that it will make it harder to brute force our keys.
* If you want to set an expiration date for your key you can for added security, however I set mine to never expire, so that anyone can reach out without worrying if the keys are still valid.
* Now you will enter your name and email. This is like your public contact card, so it's best to use the email you will be using to send your encrypted emails.
* Comments are optional, I didn't put one in.
* Next it will prompt for a passphrase. It is extremely important to choose a very strong password, as this is how you will be protecting your encryption from the public and any potential attackers. A good resource on strong passwords can be found on the [Electronic Freedom Frontier website](https://ssd.eff.org/en/module/creating-strong-passwords).

The output should look something like the following:

```
gpg: /home/user/.gnupg/trustdb.gpg: trustdb created
gpg: key XXXXXXXXXXXXXXXX marked as ultimately trusted
gpg: directory '/home/user/.gnupg/openpgp-revocs.d' created
gpg: revocation certificate stored as '/home/user/.gnupg/openpgp-revocs.d/XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX.rev'
public and secret key created and signed.

pub   rsa4096 YYYY-MM-DD [SC]
      XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
uid                      John Doe <john.doe@example.com>
sub   rsa4096 YYYY-MM-DD [E]
```

The important thing to take from this is the 40 characters listed after the 'pub' label. This is your public fingerprint, which will allow others to verify that the public key they import later is the same that was just created. It's important that you back this up, and keep it in multiple places so there is no doubt that it's authentic.

## Export and Upload Public Key and Fingerprint

Next step is to export your public key so you can share with others. Exporting the pub key to a file is as simple as running the following command:

```
gpg --armor --export john.doe@example.net > ~/Desktop/johndoe.asc
```

That will put a file onto your desktop that contains the public key that is safe to share with others. The best way to share that is to upload it to a public key server, like the [OpenPGP Keyserver](keys.openpgp.org). Only step left is to share your fingerprint (the 40 character sequence from earlier) so that others can verify the integrity of your public key file. It's smart to place them in multiple places so that your bases are covered in case of a potential hack.

## Import A Friend's Public Key

To be able to write a message to someone that only you and them can view, you will need to import their public key. It's pretty simple actually. Once you download their public key file, just run:

```
gpg --import janedoe.asc
```

After it is imported, just run the following command to double check their fingerprint to ensure the key is authentic.

```
gpg --fingerprint jane.doe@example.com
```

The output should look something like below. The 40 X characters will be their fingerprints.

```
pub   rsa4096 YYYY-MM-DD [C]
      XXXX XXXX XXXX XXXX XXXX  XXXX XXXX XXXX XXXX XXXX
uid           [ unknown] Jane Doe <jane.doe@example.com>
sub   rsa4096 YYYY-MM-DD [S] [expires: 2021-10-25]
```

## Encrypt and Sign Messages for Sending

Writing an encrypted email is fairly simple as well. You will run another command, and the output will ask if you trust the keys of the person you are sending to, then you will type your message. Once you are finished with your message you will enter CTRL+D to finish, at which point you should be prompted to enter your encryption passphrase. The command to enter looks as follows:

```
gpg --encrypt --sign --armor --output ~/Desktop/encrypted.asc --recipient from@example.net --recipient to@example.net
```

There's a lot of flags on that command, so let's take a look at what each one does for us.

* `--encrypt` tells gpg to encrypt a message. Pretty straight forward.
* `--sign` tells gpg that we want to sign the message. Also straight forward, it's typically used in conjunction with `--encrypt`
* `--armor` tells gpg to create ASCII armored output. The default behavior is for gpg to create the binary OpenPGP format.
* `--output` is the flag that will signal that the next input is the file path where we want our encrypted message to save to. In the example above we have saved it to `~/Desktop/encrypted.asc`
* `--recipient` will flag that the next input is who should be able to decrypt the message. It's not necessary to put both yourself and your friend as recipients, however doing so will allow you to decrypt the message later in case you need to revisit the conversation.

After this process is finished, you can attach the output file to an email to your friend. Once received, your friend can then decrypt the message to see what you have written.

## Decrypting a Received Message

Once your friend has viewed your message and has followed similar steps to encrypt a message back to you, you will be able to decrypt and view the message with the following command:

```
gpg --decrypt ~/Desktop/encrypted.asc
```

And you will be able to see the encrypted message in your terminal output.

## Wrapping Things Up

So that is the basic how to of encrypting an email for only one or two people to be able to see. As I mentioned before, if you were handwriting a letter, the feeling that one will need an envelope to hide the letter contents is a big indication that you should encrypt it if it is an electronic mail letter.

You should practice this! You can find my public key [on my website](/res/jacobwestall.asc), or on the [OpenPGP public key server](https://keys.openpgp.org/search?q=jacob%40jwestall.com). Download it, import it, and write me an encrypted message! Thanks for reading.

_Note: this guide is [open sourced](https://gitlab.com/jlwtformer/website/-/blob/master/src/email-pgp.md), so if you find a mistake that you feel needs to be corrected, please do not hesitate to submit a change request to have it addressed. It is important that privacy and security guides such as this one are peer reviewed on a regular basis._