# Let's Make a Video Game - The Magical Series Introduction

_Friday, July 9, 2021_

A now long standing hobby of mine has been programming small useful applications. Most times this comes in forms of scripts to automate small tasks, or in the case of my 9-to-5 I have created entire applications to automate tasks for my team. While I find scripting and programming interesting, the bulk of it is just using tools to make other tools. As I said to my boss when he inquired about this hobby, I need something to create, a goal to reach before I start coding. It's almost impossible to just sit down and start coding hoping something cool will come out, much like it's impossible to sit down with a hammer and no plan and expect a nice cabinet to appear out of thin air.

That's when I turned towards my biggest source of inspiration to find my next goal. My children of course, is that inspiration that I am referring to. My oldest is at the same age that I was when I started to get into video games, and if the time they spend on their tablet says anything, I'd say they are just as into gaming as I was.

But building a full fledged video game takes a tremendous amount of time and effort. Being that I have never made one, I have resolved to start where the greats started, with a text based adventure. While I never got into the legend that is The Oregon Trail, it would serve as the inspiration for in game mechanics for the game we will be making in this series. Yes, I do plan on sharing all aspects of creating this game with all of you.

Like I said before, we'll need an obtainable goal to reach in order to effectively create our project. While The Oregon Trail will be our inspiration for mechanics, we will want to come up with a story of our own to make the game feel unique. When talking about making a game with my oldest, it was decided that the game we will make is a medieval adventure game, where the main character is on a quest to save their beloved unicorn, named Magical, from the evil king.

With a general goal in place, let's talk about what tools we will be using to create this adventure. The list below are the tools I have decided to use to make this game:

* Programming Language: C++
* Development Environment: [Visual Studio Code](https://code.visualstudio.com/) + Any Terminal App with a Bash Shell
* Development Libraries: [NCurses](https://invisible-island.net/ncurses/announce.html)
* Compilation Software: [GNU Common Compiler](https://gcc.gnu.org/), [Make Compilation Software](https://www.gnu.org/software/make/)
* Backup and Source Control: A free [Gitlab](https://about.gitlab.com/) or [Github](https://github.com/) repository

Alright, let's explain why I chose the tools I have so far. I chose C++ as our main programming language as it has the features of an object oriented programming language, which is essential when building something like a video game. We will be using VS Code as the text editing application to write our code, as it is a very robust app that will have syntax highlighting (if you're new to programming this will make sense in the coming tutorials), as well as some intelligent code add ons.

We will be using the bash command line environment for all of our commands, as it is the one I am most familiar with. Note to macOS users: the default shell for the macOS terminal is zsh, which is able to process most of our bash commands, so you don't have to go through the trouble of setting up bash if you don't want to. On the other hand, Windows OS does not come with a bash or zsh shell by default. Windows uses a specific shell called PowerShell, which I am not sure will be able to run the command line tools we will use. To follow along you will most likely need to [enable WSL 2](https://docs.microsoft.com/en-us/windows/wsl/install-win10) and follow along with the Linux instructions for our command line procedures. If there are any complications with this please reach out and let me know. We'll get more into the other tools when the time comes to use them.

Alright, before we wrap this post up we need to talk funding. I have stated before that I will not require any funding to run this blog, and have provided donation methods as a way to show extra support if you feel you as the reader want to. This series will be a little bit different. Because building this game and teaching you all how I do it along the way is going to take a lot of time and effort, I will be requiring that I have at least a $20 a month income through my LibrePay account to continue this series.

I hope that you find this series interesting, as I look forward to continuing this endeavour with you.