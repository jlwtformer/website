# My Thoughts On The Controversy Between Linus Sebastian and Pop!_OS

_Thursday, November 18, 2021_

So about a month or so ago, Linus and Luke from Linus Tech Tips and Floatplane respectively challenged one another to use Linux on their home computers for one whole month. Their reason for doing this is to show how far Linux has come along in terms of being a viable replacement to Windows in terms of gaming. This is an interesting challenge, as there are a lot of people who are into gaming who do not trust Microsoft when it comes to privacy or security, and are itching for an alternative to Windows.

In the midst of this challenge, something pretty peculiar happened on Linus's side. Now Linus had claimed that he was trying to approach this as an average user, or someone who didn't know a whole lot about technology. But this is kind of a fallacy from his side, because he actually has used Linux before when reviewing things such as System76's Thelio Desktop. He's also not a typical user as his personal computer includes hardware that will present unique challenges that a typical user wouldn't have.

He did spend a good portion of the video venting his frustrations on the experience of Googling what the best Linux distribution (distro) was for gaming. And I don't think most people would actually do just that. Most people would probably just go straight for Ubuntu because it's the most popular in the eyes of the average consumer.

What he ended up picking was a distro by System76 called Pop!_OS. Another member of the LTT staff, Anthony, has been known to tout this distro as the best one for new Linux users in the context of gaming. LTT had even previously made a video where they pitted Pop!_OS against Windows in a challenge to see what's quickest to use from fresh install to playing a game. Pop!_OS beat Windows.

## The Controversy

Where the big controversy comes from is from when Linus went to install Steam onto his Pop!_OS desktop. When he tried to install it from the main app store, the Pop Shop, he was met with an error message that he couldn't install it on his system. He then opened up a command line and tried to install Steam from the standard user repositories (repos). When doing this, he was met with the same error message, but also with a prompt to type "Yes, I want to do this" to attempt the install anyway. He proceeded to type the confirmation, and then got surprised when his system broke just like the error messages said would happen.

Linus can go on all day about how this is a bad user experience, but he's Linus Sebastian. He should know what those messages meant, he should know not to just breeze through error messages like he did. If you want to argue that he's doing this through the lense of an average user, well and average user would not take the risk of potentially breaking their computer.

What happened after that in the video is that Linus decided to give up on Pop!_OS and move over to Manjaro. This is actually something I find reasonable and something an average user would do. I know I have done the same where I broke a system, and didn't want to take the time to fix the system.

## The Backlash

What followed from this incident is that a lot of followers of LTT took to twitter and began harassing the System76 team for the bug. The backlash over the incident was taken out of hand. It got to the point where the lead developer, Jeremy Soller, made his Twitter private to avoid getting hate for the whole ordeal. Another developer from System76, Michael Murphy, explained that when the issue happened, not after it went live on YouTube, it was fixed within a day yet the team was still getting hate for it weeks afterwards.

The response from System76 is one of the reasons I believe they are the best distro for new users. The head developer Jeremy made a thread on Twitter detailing what the issue was, what caused it, and how it was fixed so quickly. I tried to find the thread to include screenshots but it looks as if that thread is no longer public.

A rough explanation of what happened was that Steam relies on a 32-bit software library to run properly on Linux. When Linus went to install Steam the suspect library was absent from the main user repos. With this missing library, attempting to uninstall Steam caused a dependency loop that uninstalled the entire GUI of his desktop.

If you follow Anthony from LTT, you'd have seen where he noted that System76 actually listens, and had the issue resolved within a day. They not only fixed the missing library, they also made a band-aid patch to prevent users from bricking their system from the command line like Linus did. They've also begun work on creating a more robust way to do this, and are actively upstreaming the patch all the way up to Debian Linux.

It's interesting that community members put so much pressure on System76 regarding this issue. If something similar happened on Windows, how long would it actually take Microsoft to fix it? With Pop!_OS being open source, a community member could have suggested the fix, and System76 could have patched it through even sooner.

That's something else that System76 deserves recognition for, how they manage to push out bug fixes and updates scary fast. I'm currently running the beta channel of Pop!_OS on my laptop, and at the time of me writing this Pop!_OS is on Linux Kernel 5.15, which is more up to date than even Arch Linux currently offers. And so far, it's been rock solid to use. Granted I'm running a beta build, and will inevitably run into bugs and issues.

## Tangential Thoughts

While looking into this whole topic I came across a comment on Reddit that something along the lines of "Don't make your users the beta testers!" Which is absolutely hilarious to me, because that's what even Microsoft is doing these days.

There is a YouTube creator named Barnacules Nerdgasm, who worked at Microsoft on the Windows test team for decades. He was even part of the team that shipped out Windows XP. He was laid off from Microsoft in 2015, and at the time made a video explaining that Microsoft laid off their test teams in favor of having the end users discover and report bugs. The issue with this is that even when a bug is reported to Microsoft, those fixes won't see their way into Windows for a while, unless you are on the Insider Previews.

## Wrapping Up

It's nice to see Luke and Linus put a spotlight on Linux. For those users who love gaming and are wanting to ditch Windows, Linux is ultimately a good option. This challenge is also good, as it will show game developers that Linux is a platform that they need to be taking more serious.

But at the end of the day, you shouldn't be using Linux because it's not Windows or macOS. Computers are ultimately tools, and you should be using the software on them that best accomplishes the jobs you are setting out do. Whether that be to design a plane, or pretend you are flying one in a video game.

I personally still use Pop!_OS. Mainly because I really enjoy the development team, and the whole community behind System76 behind the scenes. They are all really great people, and are actually really approachable if you take the time to be a decent human being about it. 

In almost a decade of using Linux, it's one of the most stable distros I have used, even on the Beta channel.

Thanks,  
Jacob