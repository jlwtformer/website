# The Linux Distribution You Should Switch To

_Tuesday, October 5, 2021_

So there's been a movement in the last few years that has seemed to pick up more and more steam as it goes. That movement is to switch one's personal computing over to Linux, whether it's full time or only partially. While this seems like a large task to take on, it's not entirely unreasonable.

If you aren't comfortable installing a new operating system onto your personal computer, which isn't uncommon at all by the way, you can find plenty of computer makers who sell computers with Linux preinstalled. Some retailers, such as Purism, System76, and a few others have been doing it for years, and even create custom Linux distributions specifically for the hardware they sell (more on that further down).

If you are comfortable installing your own operating system (OS), then you'll quickly notice that there are a LOT of options to chose from in terms of distributions of Linux. What's a distribution? Well to answer that you first need to understand that Linux is actually not a full fledged OS, but just the kernel. A computer kernel is the part of the OS that interacts between the applications/programs running and the physical computer hardware.

To make a full OS, companies and/or communities will bundle together different (typically open source) software that will serve the full purpose of a user interface and all of the applications a user would expect to find in a computer. That is where the term Linux Distribution (distro) comes from. There are about as many distros as there are countries in the world, plus a large number more as forks (copies with further enhancements).

## So Which Distro Should I Use?

Well before you fully switch, you need to make sure the application suite you use on a daily basis will work under Linux. Typically professional editing suites, like the Adobe family of products, and the Microsoft Office suite do not run natively under Linux. There are some more technical ways to get them to run, however they are not official or supported by any one in any official capacity, so those should be left for those who are willing to experiment with the software they run.

If you find yourself needing to keep a Windows or macOS install for certain applications, then you could try a dual boot solution. I won't go over how to do that in this post, but may in a future post if enough people reach out and ask me to.

At the end of the day, the answer to the question of which Linux distribution you should use depends on how you want to use your computer. If you know how you want your computer to work and how you want it to look, then you should go look at Debian, Fedora, openSUSE, or Arch Linux. Each have a unique method of installation that lets the user pick and choose which software they want installed on their system.

If you are not sure how to pick, then we'll take a quick look at one distributions that sells itself as a user focused Linux distribution, and why I picked it.

## Pop!_OS

The one distribution that I have fallen in love with is System76's Pop!_OS. It's the one that ships preinstalled on all System76 computers, and is actively maintained by some awesome folks.

To start, they have an extensive [road map](https://support.system76.com/articles/roadmap) that lays down exactly what they plan to achieve with their distribution. They have a well defined [goal](https://support.system76.com/articles/pop-os-development-approach/) with their distribution to cater to makers and creators. In doing so, they have put together a great distribution that puts the user first in all of their decisions.

When I said it was actively maintained by some awesome folks? I wasn't kidding. System76's principle engineer, Jeremy Soller, is pretty active on his [Twitter account](https://twitter.com/jeremy_soller) where he often interacts with users and developers alike to help keep community engagement high, and even run polls on where the future of the distribution should go.

While the goal of this article wasn't meant to show you how to install Linux, I feel the need to point out that the Pop!_OS installer puts the option to encrypt the computer hard drive front and center, instead of in the corner in a small check box. This is really important, as having an encrypted hard drive is essential for securing your private information on your computers.

## Conclusion

At the end of the day, it is a smart and very recommended move to switch your personal computing over to Linux, if it is reasonable with your computer use case. I strongly believe that Pop!_OS is the way to go if you are looking to switch. If there is enough interest, I can do an install guide as well as run through a few tweaks that I have done to personalize my desktop. Just let me know if you want to see that kind of content!

![My Pop OS Desktop](/res/pop-desktop.png)

Thanks, Jacob
