# From macOS to Windows 10

_Saturday, July 17, 2021_

I am doing it again, I am changing my mind about how I use my technology and what morals I hold myself to in terms of software licencing. Essentially, I came across [this video](https://www.youtube.com/watch?v=84sCLhy-rw8) from Sun Knudsen, and it made me rethink my stance on computers, privacy, security, etc.

The big point that Sun makes in his vlog is that trying to stick to only open source/free software and getting the most privacy no matter where you go will drive you mad, and that it's not all about being open source. And honestly, he's right. Unfortunately there is no such thing as fully private software, and trying to live your life in the modern age as such will harm your mental health, as it has mine before. From what I have been able to gather from his video, the solution does not rest with what tools you are using, rather how you are using the tools you've got.

And as I stated in my blog post about [how I liked using macOS](/blogs/how-why-macos.html), Linux was a great solution but still was not a my endgame, as there was a lot of first party software missing from the ecosystem. While what I needed was available for macOS, the fact that I was running it in a not so optimized virtual machine meant that I wasn't able to use it there as well. So where did that leave me? Back to using Linux with a Windows virtual machine that could be tweaked for proper performance. Or, just give up charade and use Windows.

I chose the latter, mainly be cause dealing with setting up virtual machines, updating everything, and trying to keep it all working is just too much work. Besides that, I use Windows for work so it's not like I am not familiar with the system or how it works. I did say I liked that macOS was a nice blend of a UNIX environment with the proprietary bits. And while Windows is far from UNIX based, having the Windows Subsystem for Linux means I do have access to the UNIX tools I would regularly use, if they were not already made natively available for the Windows system.

But on the subject of keeping private and secure, I don't plan on giving that up. Privacy is a fundamental right for everyone. It's not about what you have to hide, but about what you have to lose, and unfortunately for my threat model faltering in my security/privacy attempts means my family will suffer. So I will be taking measures to protect myself on Windows, namely starting with Techlore's [awesome guide](https://www.youtube.com/watch?v=vNRics7tlqw) on how to do just that.

## Why Windows 10 and not Windows 11?

This seems to be a valid question. For starters, Windows 11 hasn't been fully released, so there's no telling if the tools I will need to keep myself safe will be available yet. Secondly, Windows 11 decided that it needs to have hardware from the last few years to work properly. Unfortunately I nor my family are made of money, so we just don't have any hardware that is officially supported. Yes there are ways to build a Windows 11 installer that will bypass those restrictions, however most of those methods involve signing up/into the OS with a Microsoft account. And that is just not something I will be willing to do.

---

So that is my existential computer moment of the week. I really do need feedback on how this blog is being handled, so please do [reach out](/cont.html) to me with any thoughts you have, good or bad. I want to write about what I like, but I also want to write about what you as the reader like as well. Thank you for reading, I hope you have a wonderful day.