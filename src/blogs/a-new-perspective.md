# A New Perspective

_Saturday, January 1, 2022_

Not that long ago I came across the YouTube channel for CGP Grey. Like usual I decided to watch every video of his that I could, and came across one that really left an impression on me. That video was the one about having a personal theme.

Now I won't go into too much detail, you can watch his video for that, but I'll explain what he means a little bit. Your theme is the way you shape your goals and the way you direct your life. One of his suggestions in that video was to not pick a New Years Resolution, but rather a New Years Theme. Something to help guide your year down a path you'd be proud of. And that's something I'm going to do this year. But what theme have I chosen?

## Focus

This year I want to focus. Often it seems I'll get started on a project, get into working on it, and then get distracted and the project fizzles into nothing. And this doesn't just apply to hobby projects, it happens with situations at home as well. I would like to focus more on seeing my projects through, plain and simple.

So with that said, I wanted to put my plans out there for the handful of projects I want to tackle in 2021. The projects are (in no order):

* Boba Fett EVA Foam Armor Build
* Iron Spider 3D Printed Helmet
* Progressive Web Apps with Dotnet and C#
* Programming with Rust
* Workbench Build
* Starting to build my perfect Drumset

There will still be a few one offs that I will do as well, while giving my best shot at focusing on the above projects. I don't plan to finish all of these projects this year, but at the least I hope to start them. And yes, they will all come with vlog entries as I progress with them.

## Focus In Other Areas

As I alluded earlier, I want to improve the focus in other areas in my life as well. This will include rethinking how I use my technology, and how I handle my projects. This will include writing with paper and pencil when planning my projects and doing my concept drafts. This is an interesting approach that I'm excited to get into.

So what do you think of my plans? Are you excited for what's to come? As always you are more than welcome to reach out and let me know your thoughts.

Thanks,
Jacob
