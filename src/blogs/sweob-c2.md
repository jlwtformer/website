# Fan Fiction: Star Wars Echoes of the Beyond - Chapter 2

_Wednesday, June 17, 2021_

Continuing with last week, here is Chapter 2 of the Star Wars Fan Fiction written by myself, with story by myself and my brother-in-law Josh. I had written three chapters total of this story before I was sidetracked with other projects, so please do share this and reach out to me if you want more. I will happily write more chapters if there is enough interest.

---

As the Jedi star ship extended it’s landing gear to settle on the Naboo Visitors dock, Siddhartha only felt his weariness grow. The beautiful landscapes of Naboo amazed Siddhartha, as he had never seen such beauty from his origins on Lothol or his home on Tython. The beauty was still not enough to distract him from his gut feelings.

“Welcome to Naboo, my apprentice.” Master Quinn broke the deadening silence. “Tell me again about your hesitance, what do you have a bad feeling about?”

“I can’t pin point it, but my entire body feels as is it’s being pulled to leave as soon as I can. It’s like an invisible being is tugging at me, and trying to tell me to run.” Siddhartha replied.

“I see...” Master Quinn continued, “It sounds as if the Force is reaching out to you, Siddhartha.”

“It is?”

“It very well could be. The Gungan wars that ravaged these lands we are going into caused great suffering, and thus disturbed the balance in the force. What you are feeling is the suffering from these events that upset the living force. Be mindful of your feelings, as we have come to rebuild what was lost, and the compassion we feel for the living force will be our guide.”

“Thank you Master.” Siddhartha replied, a gentle smile adorning his lips. The feeling inside himself only grew, but he felt the strength in his master’s words hold his spirit up, and give him the strength to cope.

“Very good then. Let’s be on our way, we do not want to keep the Minister waiting.” Quinn said. With that the two Jedi departed the ship and made their way to the Welcoming Hall in the Grand Naboo Center.

---

Entering the Welcoming hall continued the visual overhaul of Siddhartha’s senses. The grand golden arches that connected the ivory walls lined with statues of what he assumed were important political and cultural figures. At the end of the hall resided a large desk with a well dressed assistant sitting behind the counter top. As Master Quinn and Siddhartha approached, the assistant looked up and greeted them with a welcoming smile.

“How may I help you?” The assistant asked.

“We are the Jedi here to assist Minister Van Dari with the reforestation of the Gungan Forest.” Master Quinn replied.

“Oh yes, we’ve been expecting you.” The assistant replied back. As the assistant turned to reach for the telecom to notify Minister Van Dari of the Jedi’s arrival, Siddhartha noticed that they were moving in a very stiff manner. Siddhartha pastured if this style of movement was typical of native Naboo inhabitants, however it reminded him of how stiff the movement of the Jedi Temple Droids were back on Tython. Master Quinn silently shared the same thoughts.

As the intercom beeped to life, the assistant spoke very firmly into the microphone, “Minister Van Dari, you have visitors to see you.” The assistant then sat back up and presented an empty smile directed towards Master Quinn.

Master Quinn had felt the same uneasiness that his apprentice had described to him. While he believed that the disturbance in the force that they felt was a product of the history in the nearby area, he began to wonder if it was not the force trying to warn him of an imminent danger.

“Welcome! My esteemed guests!” Shouted an enthusiastic voice from across the great hall. Both Master Quinn and Siddhartha turned in surprise to see where the rhapsodic welcome had originated. As they turned they spotted a short and pudgy man walking with arms open towards them.

“Welcome... Welcome... Welcome...” Minister Van Dari continued as he walked closer to the two Jedi. Van Dari strode his way across the hall allowing his long jacket coat to slide along the floor behind him, with his shoes making a particularly annoying squeak. On his approach, Master Quinn bowed as a form of respect, in which Siddhartha quickly followed once he noticed his master doing so.

“Hello Minister, I am Jedi Master Quinn, and this is my apprentice, Siddhartha.” Quinn extended his arm out to Siddhartha, placing his hand on his shoulder. As the Minister shifted his focus towards the young padawan, Siddhartha felt the uneasiness in his stomach increase dramatically. He had never seen anyone from either Lothal or Tython wear an outfit as exuberant or lavishing as what Minister Van Dari was wearing. He tried to reassure himself that this was normal for Naboo Ministry, and that he was just adjusting to the unfamiliar.

“Why this is the great Siddhartha,” Minister Van Dari spoke to Siddhartha, “I have heard nothing but great things about you from my colleagues.” The Minister spoke with a slight slur that could be perceived as laziness in pronunciation. “I am eager to see what wondrous things you will be able to accomplish in helping us restore our lovely native lands.”

“He will do good work, just as he always does.” Master Quinn said reassuringly as he lifted his hand from Siddhartha’s shoulder.

“Very good..” The Minister replied. “Then let us be on our way.” As he finished his sentence the minister turned to lead the two Jedi through the Naboo Welcoming hall, and out to the Capital City commons.

As they strode across the commons area heading towards the Minister’s palace, Siddhartha continued to be taken aback by the wondrous beauty of Naboo’s landscapes. But no matter what wonders he witnessed, they felt tainted when accompanied by the stomach turning coldness he felt in the atmosphere around him.

---

The Grand Ministry of Natural Preservation was a forefront of Naboo culture. The planet’s people prided themselves on living on the most beautiful planet in the known galaxy. Jax Smithson knew this all to well, as a mechanic to the Naboo commoners and visitors.

Jax had lived his entire life on Naboo, being raised a mile outside of the Planet’s premier trading post. While he made an honest living fixing those ships that came through on their various jorneys, he found his joy in collecting old mechanical scrap and rebuilding what was once lost over time.

In the shop he resided in for most of his days, he did his best to hide his hobby. However, the shop owner always found out and disposed of the junk that Jax collected.

“Hurry up down there!” A Twi’lek spice trader screamed to Jax from his cockpit. “I have 6 more deliveries to make before the end of the week!” Jax looked up at him with a soul piercing stare. He was normally fond of meeting all sorts of alien culture that did not originate from Naboo, but Twi’leks were not his favorite.

“I’m working as hard as I can! Next time don’t run over a shaak on your way in.” Jax yelled up in reply. As he was returning his attention to the busted section of the transport’s fuel lines, he felt a sharp breeze on the left side of his head, just as a metal knife slammed into the ground next to his foot. He looked down at it, and cursed under his breath, “Crazy Twi’s...”

He tightened the last bolt to secure his work, and turned to set his tools down on his workbench. As he turned back to announce his completion the Twi’lek driver howled, roaring his engine before taking off out of the maintenance bay.

“Hey!” Jax screamed as he chased the ship on his feet, stopping at the maintenance bay door. “You didn’t pay you con of a -”

“JAX!” Screamed Hal Namor, the owner of the maintenance shop. “What do I have to keep telling you?! Pay comes BEFORE the work!”

“I know! He said he was gathering his credits while I fixed his ship!”

“Jax this is the third time this month that this has happened on your rotation. If it happens one more time, I’m going to have to send you back to the trash collections duty.” Hal looked scornfully at Jax, sending a chill down his spine. “Which I’m sure you’ll love, since I found more of your garbage hidden in my storage bay.”

“It’s not garbage.” Jax defended himself. “It’s spare parts, so I can rebuild the Model Y-”

“I don’t care what you think you can build from it.” Hal interrupted. “It’s junk, and belongs in the trash, just like you.” Hal turned and returned to his office.

Jax looked outward at the blue Naboo sky. He longed more than ever to leave the maintenance shop and his home, to search the stars and find new adventures. For the time being he had to settle, knowing he had no means of leaving Naboo any time soon.

As the thought crossed his mind, he noticed a gleam from the garbage ship coming to empty the shop’s build up of junk. He knew that Naboo did not keep its trash on the planet proper, but rather on the world of Lothol Minor. He did not know, however, where in the galaxy that world resided. A thought crossed his mind as he watched the garbage ship unload the trash compartments.

“I belong in the trash he says...” Jax said to himself, as a desperate plan hatched in his head.

---

Siddhartha tried to keep the turning feeling in his stomach settled as his master and the Naboo Minister discussed the finer details of their mission. The hologram of the Gungan forest fulled the top of the meeting table they all gathered around, prospective tree plantations scattered the virtual hills.

As Minister Van Dari droned on about the rich relationships that would be formed in the act of reforestation, Siddhartha tried to keep himself awake by staring out the window behind the minister. He stared for a moment too long, as he missed his cue to chime in from his master.

“Siddhartha?” Master Quinn questioned his apprentice, trying to regain his attention.


“Yes Master!” Siddhartha replied, startled.

“Are you feeling okay? Your face looks flush..”

“I am not sure Master.” Siddhartha replied honestly. The unsettling coldness and uneasiness he was experiencing was making it hard for the young Jedi to focus on the tasks at hand. “I feel that the long journey here has made me ill. May I be excused to the lavatory?”

“Of course.” Quinn stated.

Siddhartha rose from his chair and left the

meeting room, Minister Van Dari providing a cold glare as he exited.

Siddhartha searched for what felt like half an hour for the closest relief station. He was not familiar with the halls of the Naboo Ministry of Natural Preservation, and the fact that there wasn’t a clearly laid out map didn’t help either.

If I can’t find the darn lavatory myself, I might as well ask someone for help... He thought to himself. He then turned to the nearest door and opened it, hoping to find another receptionist to help point him in the right direction. What he found when he opened the door was the exact opposite.

Sitting in the room was a small congregation of dark figures in long hooded robes, all sat in an almost perfect circle. They were chanting a strange ritual in a language Siddhartha did not understand.

One of the hooded figures rose their head in response to the door opening, and locked eyes with Siddhartha. This act sent a spike of numbing coldness down his spine, freezing him in the moment. As others in the small congregation began to take notice of his intrusion, Siddhartha pressed the door panel and closed the door.

Immediately, Siddhartha took off down the hall in attempt to return to the meeting room that his master was in. What in the grand force was that!? He thought to himself.

As he turned the hall to the meeting room, he heard the faint sound of a lightsaber igniting. Oh no! He thought to himself. Siddhartha knew Master Quinn well enough to know that he would never activate his lightsaber unless he was in mortal danger. He quickened his pace, sliding to a halt in front of the meeting room, slamming his hand on the door panel to open the room in front of him.

When the door slid open, he was presented with the sight of Master Quinn on his knees, a look of terror adorning his face. Three large men in black cloaks, all wearing dark masks with individual designs.

“Siddhartha! Ru-” he started to say, before being interrupted by the blade of a metal sword being plunged through his back. The three figures looked up at Siddhartha, as he was turning to run out of the room.

Just as Siddhartha made it to the main corridor, a large hand found itself around his neck. The hand raised Siddhartha off of his feet, and then slammed him on his back onto the ground. Siddhartha tried to hold onto his consciousnesses for as long as he could, however it became too much, and he passed out from the pain.

---

_Earlier_

The night sky of Naboo was just as breath taking as the day time on Naboo. The capital city had a light ordinance to preserve the view of the Galaxy during the night hours.

The royal guards protecting the Ministry of Natural Preservation during the night rotations were easily bored of their jobs, as no one was ever active on those hours. Usually they passed the time by playing games of sabacc, however on this fateful night their game was cut short by the alert of incoming visitors.

“Who would be visiting at this hour?” One guard asked to another. The second guard only returned a worried and puzzled look, grabbing his royal rifle to head to the Ministry docking bay. The first guard glared at the security feed for a moment longer, and then followed suit.

The guards arrived at the entrance dock at the same time as the mysterious ship had finished landing. The ship was a small vessel, no larger than a common Carrack-class light cruiser. It extended its landing pad, and from it emerged a towering figure in a dark hooded robe.

“You do not have authorized access to this sector, state your busine-” The first guard began to demand, before feeling his neck close in on itself.

The hooded figure lifted one gloved hand up from his robe, holding the two guards in the air by their necks simultaneously. As the figure tightened its hand into a fist, the guards necks snapped, and they fell limp.

The figure strode past the now dead guards laying on the ground, making its way into the Ministry. As it did so, it pulled a small cylindrical handle from its robe, and pressed a button on it to ignite a meter long blade of pure red light.

---

So there it is! Chapter 2 of Star Wars: Echoes of the Beyond. Did you like it? Do you despise it? Please reach out and let me know! If you did like please share this with all of your friends and others who you think might like it as well, so that I can get more exposure and have more motivation to finish this story.