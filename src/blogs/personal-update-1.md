# A Personal Update

_Saturday, June 25, 2022_

So it's been a good minute since I've made a blog post. I thought since I'm not as active online as much I'd give an update as to what's been going on, in case you wanted to know.

## Where'd I Go?

Well I didn't really go anywhere, just stopped getting online so often. I have a growing family, my wife is due to deliver our 4th child this August. I've been trying to play keep up with my house, children, sanity, etc. I had started making video logs, but very quickly realized the amount of work that goes into making a quality video for one to enjoy watching. Because of the amount of work needed to produce a quality video (especially if you're not experienced) I had to take a step back to focus on the family side.

Just as well, my career took a turn that I didn't quite expect. This past month I moved departments at the company I work for, now having the title of Associate Research and Development Engineer. It's one of those opportunities that I didn't quite see popping up at my company, however it did and I took that chance to follow a career path that is more in line with what I had envisioned coming fresh out of college.

## What's Going On With The World?

If by chance you are reading this long after it's been written, this past Friday was the day in American history where the Supreme Court overturned the Roe v. Wade ruling. Now I am quite scared of discussing politics, as with how divisive it has become it can very easily make someone feel unwanted or marginalized. I won't go too far into this topic other than to deliver my small peace with this event in history.

Two people who I love and care for with all of my heart would not be alive today if it were not for the protections that were under Roe v. Wade. Getting rid of it is a crass, tasteless, irrational move made by people who have lied to get the power that they are now abusing. Not much more I can say about that.

## Where Do We Go From Here?

Well I am not going to be ramping up blog and vlog updates. My focus right now is on my career and more importantly my family. I'm not stopping, but will more than likely slow down even more. I do have that project tracker so you can see what I am planning on working on in the future.

As always, I do encourage you to reach out however you feel you want to.

Thanks, Jacob
