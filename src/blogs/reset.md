# Reset.

_Tuesday, August 17, 2021_

So when I first started this blog, I had set to simply share what I found interesting. Since then, I have shared primarily privacy and security related content, with some filler in between. I did not set out to make this a primarily technology blog, but it feels like that has been what it is turning into. While I do like technology, it is not the only thing I care about.

I feel like I've spent too much time focusing on the security and privacy side, and have lost sight of some of my other hobbies. I recently used my 3D printer to print out an entire Mandalorian helmet, and the process has gone better than I anticipated. Yet for some reason I did not plan to blog that at all.

Starting now, I will make a conscience effort to write more about other topics. Cover more on the maker side of the road, maybe share a few short stories. And, whatever my readers (hi mom!) would like to see from me.

As well, I have decided to continue the Magical series without having a set donation amount tied to it. As it stands this blog is relatively unknown, and asking for a steady income at this point may not be the best of ideas. So I'll get that series rolling soon (TM).

Next, I would like to redesign my website, again. I like the simplistic static website, and want to keep it that way. But, I think it could look better. I will be looking for ideas, so please do send in ideas of how I could improve the look and layout of this website, and I'll be making those updates in the coming weeks/months.

Thank you again for reading my blog. I look forward to hearing from you and your ideas.