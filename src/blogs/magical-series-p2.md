# Magical Series Part Two: Setting A Foundation And The First Line of Code

_Thursday, October 7, 2021_

So in my last entry into the Magical Series, I discussed how we were going to make a console based game that would take an explorer through a medieval world in search of a lost pet. Since making that post I wanted to adjust our course a little bit in order to make this series more attainable for those who are newer to coding and programming in general.

The main change will be that instead of writing the game in C++, we will be writing it in python. Why python and the change? Again it's so readers of this blog who have no coding experience whatsoever will have an easier time following along, and so we don't have to dive too deep into the low level concepts of programming to explain some otherwise basic concepts. Some may have an issue with this, and that's okay. If someone reading this blog is captivated enough with the high level explanations, then I more than encourage you to research the lower level concepts from others who are more capable/experienced in the computer science field than I. Now, lets get this project rolling.

## Getting Our Development Environment Set Up

The first thing we will need to do to set up our development (dev) environment is set up a folder directory to keep all of our dev documents organized. We will begin by making an empty folder anywhere we want to keep our files. Typically this can be stored on the Desktop, or in the Documents folder. I prefer to make a folder in my user directory called Repos to keep all of my dev repositories (folders). Make sure to name the folder that of the project so we know which project it belongs to.

Next we will need to make a few folders inside of the main project folder. These will be src and docs for now. src is where we will keep the source code files for the project, and the docs is where we will keep the project documentation as we go along. When done the project structure should look like this"

_Note, folders are expressed with a trailing forward slash._

```
Magical/
	docs/
	src/
```

Now that we have a basic structure set up we can move forward with outlining the story. It may seem like a meaningless step now, but having a skeletal structure in place early will keep us organized once our file counts grow significantly.

## Creating The Story

The first document we will create will be a markdown file called 'story.md'. In this file we will keep a rough outline of the story we want to follow in the game. We are making it a markdown file so that we can add some formatting, without having to make a full on word document. We'll be putting it in the docs folder, and will have the following outline:

```markdown
# Magical Story

Chapter 0: The Tutorial
	- Player awakens in their cabin
	- Their pet begs to go out fo food
	- Player will take pet to get food
	- Player sees a hurt bystandard on the road
	- Player has option to help or ignore them
	- Player runs into a royal caravan
	- Caravan takes player's pet

Chapter 1:
	- ...
```

The full contents of my story file will be found on it's git repository page (we'll talk about setting that up later on, link TBA). With the full story we will be able to map out the interactions the player will have to make moving through the game. We'll set up how some actions will help out down the line, and how some will hurt the player's progress.

## The First Line Of Code

Now comes the meat and potatoes of the project. The code. As I said earlier we will be using VS Code as a text editor to write our actual code. The reason for this is it will have syntax highlighting and code suggestions as we type, making coding easier.

Now for the first line of code. Create a file in the src folder called 'magical.py'. Having the py file extension will tell VS Code that it is a python file, and will let our python run tool know it is as well. In this file we will have just the following line:

```python
print("Welcome to Magical!")
```

Simple, right? all this does is output the text `Welcome to Magical!` to our console output. Simple as that. To run the file, open up a console/terminal window in the src directory, and run `python magical.py` to run what we have so far.

So to wrap up, our project structure should look like below:

```
Magical/
	docs/
		story.md
	src/
		magical.py
```

And that is all for now. It may not seem like much, but it's a good start as we continue along with making this video game.

It is important that instructions tutorials such as this one are peer reviewed and kept as accurate as possible. If you feel I have made an error in this post, please review the [source file](https://gitlab.com/jlwtformer/website/-/blob/master/src/blogs/magical-series-p2.md) for this post and submit a pull request with corrections.

Thanks, Jacob
