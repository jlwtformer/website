# 3D Printing is Freaking Awesome

_Wednesday, July 7, 2021_

This past Father's Day my family got me an amazing gift, that of a 3D printer. And having used one in the past I can say for sure that this gift was one of the best things I could have gotten for Father's Day. Despite my children thinking it's a free toy printer, I have been making good use of it, and making some cool stuff.

3D printing is also commonly referred to as additive manufacturing. That is a manufacturing process where a product is developed by adding material on, as opposed to removing. Think of it as the complete opposite of creating a sculpture. With that, an artist will start with a large block of ivory, and will chip away the material they do not need as a part of the final art piece. With additive manufacturing, the material is added on in small quantities at a time, to build up to the final piece.

I've used 3D printers before in some cool ways. As a part of my senior design project in college, I helped a great group of talented engineers design and build a Low Reynold's Number class airplane (pictured below). The team captain, Cameron, came up with the idea to add winglets to the ends of the plane wings to reduce vortex drag at the wing ends. While teams in previous years had done similar, making the winglets out of wood like the rest of the plane was would have taken a significant portion of our weight budget.

That's when the decision was made to use the University Library's 3D printer to create the winglets. Doing so allowed us to make a less dense part, as a 3D printer can print an object as an entire solid part, or with a lesser in fill. that basically means that instead of making it a solid block, the outer walls of the object will be supported internally by spaced out supports. That extra room on the inside of the part allowed us to make the winglets weigh less than they would have otherwise, saving us room on our weight budget.

![The BFOE in all of it's glory.](/res/3DP-bfoe.jpg)

Just like with all other methods of manufacturing, 3D printing something isn't the only step in creating an object. While you can print some cool objects, you still have to follow the laws of physics. What that means is that you can't just print things out of thin air, they have to be built on top of something. This is very evident when your object has overhangs. Typically to accomplish this, overhang supports will be printed with an object, as seen in the images below.

![Cura settings for an object with an overhang.](/res/3DP-cura.png)
![The object printed with supports.](/res/3DP-printed.jpg)

Depending on how you configure your print, this could lead to difficulty in getting the supports off, which leaves you with a scratched up part. I encountered this problem with the pommel of my model Darksaber shown below.

![A damaged Darksaber pommel.](/res/3DP-scratched.jpg)

Another pain point that I constantly struggle with is bed leveling. If your print bed is not set to be perfectly level with the extrusion head, it can mess up your model (also seen in the image above) or worse damage your printer. While there are plenty of good guides to bed leveling, I still find the process frustrating to say the least.

At the end of the day 3D printing is still one of my favorite pieces of home technology that exits. I have been making good use and will continue to make great use of mine, including creating cosplay pieces for me and my family.

---

If you liked this blog post please make sure to share it with others who may enjoy it as well. I do plan on making more posts about 3D printing in the future, but still need your feedback to let me know write about!