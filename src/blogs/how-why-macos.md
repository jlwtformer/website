# How and Why I Use macOS

_Saturday, June 19, 2021_

Computers have become a ubiquitous tool for the modern age. Just like with cars, just about everyone in my corner of the world owns one, and has a history with their use. Like most families in the great American Suburbs, I grew up borrowing time on the family desktop computer, which ran the most readily available copy of Windows. I stuck with the "use what it shipped with" mentality up until I graduated high school and was gifted my own laptop. It just so happened that the preview version of Windows 8 had just released that summer, and wanting a taste of what was being touted as the "future" of desktop computing I decided to learn how to install it on my laptop.

That was the beginning of the end so to speak, as now being able to change the steering wheel out on my virtual car I wanted to test drive all sorts of new car steering wheels. I began searching for what else I could use to run my computer, and stumbled across an operating system called Ubuntu. Ubuntu is a popular distribution of a computer kernel called Linux, which was endlessly fascinating. I ended up staying on Windows as it was where all the tools I needed for school ran, and didn't revisit Linux until my final year of college. From there I went a little mad with Linux, installing all sorts of OS Distributions. The thing that made me fall in love with Linux OSes was that it was completely free to use, from both a monetary and speech point of view.

Another reason I stuck to Linux so much was just how much I could tinker with it. Being able to dig down and tweak every part of my computer really made it feel like home. However something was always a miss with Linux, the dreaded app gap. A lot of the commercial software I became familiar with was just not available on Linux. Things such as AutoCAD, the Adobe suite of apps, the 800 LeapFrog companion apps for kid's toys, etc. were all missing from the Linux ecosystem. While this wasn't really an issue 75% of the time, the other 25% it was a pain trying to find comparable alternatives.

That is when I began messing with virtual machines to satisfy the app gap. I would run Linux on the bare metal of my computer, and then run a copy of Windows in a virtualized environment to access the apps I needed. That is when I stumbled across the [macOS simple KVM repository](https://github.cdom/foxlet/macOS-Simple-KVM). This repo contains tools to get a macOS virtual machine up and running in no time. Since it had been a few years since I had tried out macOS, I decided to give it an install.

And I must say, I have been pleasantly surprised with my experience of macOS so far. I was able to discover just how similar to Linux it was under the hood. It should be noted that macOS is not a Linux distribution, rather it has a custom kernel that is based off of BSD, which is also a UNIX based kernel similar to Linux. Having these tools and shell access that I loved about Linux, as well as being able to install the apps I needed Windows for, I admit that this once long time Apple hater fell for macOS.

![My desktop and laptop running the VMs](/res/setup.jpg)

## Setting Up a Useable macOS Virtual Machine

Getting a macOS VM setup is actually pretty simple. The repository I linked to a couple paragraphs back lines out how to do so fairly simply. I'm going to walk through it here, as well as show how to change a few things to tweak it to my preferred taste.

First step is to install the dependencies. It should be noted that my base system, the Operating System that I am running on my computer's hardware, is Linux (Pop!_OS to specific). The repository is setup to assume you will be running Linux as well, and while I am sure this could be done on Windows with some additional tweaking, I haven't tried to do that so I will assume that you are running Linux as a host OS as well. Next you will need a few apps that the setup is dependent on. They are as follows (copy/pasted from the repository read me):

```shell
sudo apt-get install qemu-system qemu-utils python3 python3-pip  # for Ubuntu, Debian, Mint, and PopOS.
sudo pacman -S qemu python python-pip python-wheel  # for Arch.
sudo xbps-install -Su qemu python3 python3-pip   # for Void Linux.
sudo zypper in qemu-tools qemu-kvm qemu-x86 qemu-audio-pa python3-pip  # for openSUSE Tumbleweed
sudo dnf install qemu qemu-img python3 python3-pip # for Fedora
sudo emerge -a qemu python:3.4 pip # for Gentoo
```

The next step is to get the virtual system drives downloaded. There is a script in the repo that will download the setup disk for you, which can be run with `./jumpstart.sh`. This will by default download macOS Catalina recovery disk, and you can download High Sierra or Mojave with `./jumpstart.sh --high-sierra` and `./jumpstart.sh --mojave` respectively. The next virtual disk is our virtual hard drive. We'll create it with the following command:

```shell
qemu-img create -f qcow2 MyDisk.qcow2 64G
```

This command creates a 64GB virtual disk for us to use as our main hard drive for macOS. You can resize it whatever size you need by changing the `64G` parameter. Please note that it will create a file of the size specified, so make sure you have enough free space on your actual hard drive for the virtual one. Next we are going to add the following lines to the `basic.sh` script that was downloaded with the repository:

```shell
    -drive id=SystemDisk,if=none,file=MyDisk.qcow2 \
    -device ide-hd,bus=sata.4,drive=SystemDisk \
```

This tells the `basic.sh` script to use the virtual hard disk we just created as the main hard drive in our macOS VM.

Next thing to do is to launch the virtual machine by running the `basic.sh` script! When it first boots, it will boot into the recovery disk that was pulled in by the `jumpstart.sh` script. Before we can install, we will need to launch the Disk Utility app from the menu that is presented to format our virtual hard drive for use. When the Disk Utility launches, we are going to select the disk that closest matches the size we created earlier. It won't be the exact same size, but it's the one that's closest. When prompted, we'll select the following options:

* Name: Whatever you want. I typically name mine `VirtualMac` or something similar.
* Format: Select `APFS` for Apple's Proprietary File System
* Scheme: Stick with GUID Partition Map

Then select `Erase` and let that process finish up. Once it's done, we can now quit out of the Disk Utility and begin installing macOS to our virtual hard disk! This process will take a LONG time, and the VM will restart several times, so remember that patience is key here.

Once it is done, you will be greeted with the macOS setup screen. It is VERY important to NOT sign into your iCloud or AppleID when setting up the VM! Doing so could out you as using a VM to Apple, and they will ban your AppleID. I personally do not use macOS with an AppleID/iCloud account, however if you are wanting to, you can follow [this work around to get it working without harm](https://github.com/foxlet/macOS-Simple-KVM/blob/master/docs/guide-Apple-ID.md).

There are some other things I do to make my configuration more enjoyable. Notably, [adding more RAM to the VM](https://github.com/foxlet/macOS-Simple-KVM/blob/master/docs/guide-performance.md) to speed the machine up a hair, as well as [adjusting the VM screen resolution](https://github.com/foxlet/macOS-Simple-KVM/blob/master/docs/guide-screen-resolution.md) to match that of my computer monitor. One big drawback of this setup is the graphics performance. By default, the VM will use the standard qxl video driver, which will only supply the VM with 3MB of VRAM. This is fine and all for drawing 2D objects on the display, however it is very slow at times and holds no 3D rendering capabilities. While it is possible to [pass a secondary GPU through to the VM](https://github.com/foxlet/macOS-Simple-KVM/blob/master/docs/guide-passthrough.md), I do not posses the hardware needed to do this.

This setup has allowed me to use macOS for 95% of my personal computing, which is much more than I used either Linux or Windows for respectively. With that said, I have been looking to sell some of my old computers for cheap to save up for a Macbook. While Apple's stance on the Right to Repair movement is one that I refuse to get behind, I will excuse that in order to obtain a device that is officially supported by macOS. Having an absolute view on any computer company is not a sustainable model, and the key is to compromise when necessary.

If you liked reading this content please feel free to reach out and let me know. Just as well please do share with anyone you think might like it as well, as the more the audience grows for this blog the more incentive I will have to write for it. If you want to read more macOS content from me (like how to upgrade our VM to Big Sur) please reach out and let me know as well.