# Making a DIY Boba Fett Armor: Part 1

_Tuesday, January 25, 2022_

With the release of The Book Of Boba Fett, the world's most infamous bounty hunter has come into the spotlight once again. To commemorate his recent success, I've decided that I will be making the entire armor of the titular character. Costume making of iconic characters, often called cosplay, is a long tradition with franchise fans world round. Boba Fett is a character that has been around long enough that there is more than enough resources to create your own copy of his iconic armor.

Now I have at my hands something that is a huge advancement in home manufacturing, a 3D printer. I will not be using my printer for making this armor however. Why? Because as popular as 3D printers are getting, not everyone has one, and I want this series to be as accessible to everyone as possible. The material I will be using is things you should be able to find at a local craft store.

With that noted, where to start? While most would assume the helmet, I am actually not beginning with that part. I will be starting with the chest/abdomen plates and the shoulder plates. These are pretty straight forward, and is part of the armor Boba wears 100% of the time he's on screen, as opposed to his helmet which gets removed often. With that said, let's get into the meat and potatoes of making our armor.

## Materials Needed

* Templates
* EVA Foam (5 mm)
* Silver Sharpie
* Box Knife/Scalpel
* Heat Gun/Heat Source
* Epoxy or Craft Glue
* Brushes
* Primer
* Spray Paint
* 220 Grit Sand Paper

The materials are nothing too expensive. Make sure that with the knife you are using proper precautions not to injure yourself. When making my first Mandalorian Helmet out of foam I did nick myself with my box cutter. Safety first kids.

Getting started, the first step is to trace the armor templates onto the foam with the silver sharpie. It helps to use sewing needles (if you have some) to pin the templates to the foam so it won't move while you are tracing it. You should trace them in a way that will save the most foam, as we'll need the extra for later pieces of armor. After tracing, you'll need to cut out the armor pieces. Make sure to cut on the inside of the traced line, as that is where the edge of the paper template actually is, otherwise it'll be slightly bigger than you want.

Next is to form the foam. This will be done by heating the foam up with the heat gun. Since I don't actually own a real heat gun I just used a space heater I use in my garage to heat it up. Once the foam is just barely too hot to touch, you will press it around a rounded surface (most people use the end of a ladle or an anvil, I used mt leg) to shape.

After forming the foam you will need to seal it. What this does is keeps the primer/paint from soaking into the foam and looking spongy, which is the opposite of trying to mimic metal like we are trying to do. I would recommend using craft epoxy to do so. Make sure you use a proper brush with the epoxy, as I did not and had a hard time applying the epoxy to my shoulder plates. Because of this I had to use craft glue on the chest/abdomen plates, resulting in those pieces losing their form and flattening out.

The next step is priming and getting ready for paint. It's important that you get an even coat across all surfaces so that the paint will stick evenly. After the first coat of primer is dry, you should lightly sand all surfaces with 220 grit sand paper. Then add another even coat of primer, and sand lightly again after drying.

You're now ready for paint! Just like the primer, make sure to get even coats across surfaces. Now typically you would want to use a sealant to keep the paint on the primed foam, however for my build I opted to skip over that. Why? Well I wanted the paint to chip and fall off naturally so it has more natural weathering to it.

So now we're done! But where's all the pictures of the hard work? Well this was just a supplementary write up to the accompanying vlog post, which can be found on either my YouTube or TILVids page. I had a lot of fun making these, and hope it was an educational adventure for you as well.

Thanks,
Jacob
