# Fan Fiction: Star Wars Echoes of the Beyond - Chapter 1

_Wednesday, June 9, 2021_

A long time a go... okay maybe about two or so years ago, I had the great idea of writing my very own Star Wars story. I got the idea after watching Star Wars Episode 9: The Rise of Skywalker, it had reawakened an old love of the franchise that I had veered away from in my impressionable years. When discussing this concept with my brother in law Josh, he had about a million great ideas that I just couldn't come up with on my own.

I had written around three chapters when COVID decided to make it's great entrance into the Americas, and flipped everyone's lives upside down. After that time I stopped working the story. Because I wrote them, I'd like to share what I have, starting out with Chapter 1.

So without further adieu... Star Wars Echoes of the Beyond - Chapter 1. If you like it please do let me know and I will post more chapters, maybe if it gets enough attention I will continue to write on the story Josh and I came up with.

---

_Breath. Concentrate. Push._ Jedi Padawan Siddhartha reiterates his master’s words in his head. He struggles to use all of his strength to keep the 10 ton boulder floating above his head. He knew that if he loses concentration for even so much as one second it could fall, and end his life.

He strains, and begins to move the large hulking rock from above his head, attempting to set it down with care on the grassy plain in front of him. Try as he might, the boulder proved too heavy, and he was forced to drop it ungracefully. As the rock falls to the ground with a tremendous thud, Siddhartha falls to his knees. He struggles to catch his breath, having used every last bit of his energy moving the boulder from the hill top to the valley he resides in.

“You have done well, young Jedi” a voice proclaims. Siddhartha turns his head to face an older, tall and slender gentleman standing by the treeline adjacent to the field Siddhartha and the boulder reside in. The gentleman nods in approval, with a satisfied smile adorning his lips.

“Thank you Master Quinn.” Siddhartha replies. The breeze rolling through the hillside cools Siddhartha, blowing across his bare head. He stands up, and brushes the grass from his orange robes.

Master Quinn approaches him, remarking, “I am very glad you volunteered to join the Jedi cause. Your natural gifts and talent in the force will serve the galaxy quite well.”

“Thank you,” he replies, “but Master?”

“Yes Siddhartha?”

“Planet Tython is only so large. When will we begin to use the force to restore the nature of the rest of the universe?”

“Sooner than you might think, young one.” Quinn replies, with his peaceful smile beginning to grow. “I have received a transmission from the Minister of Natural Preservation on Naboo, it appears word of your talents has made its way across the galaxy.”

Siddhartha approached Master Quinn, a smile growing on his face, “Really? How could they have known if I’ve only ever lived on Tython since joining the Jedi?”

“Many masters of the Jedi arts have viewed your training with great interest. When you volunteered to join, the council did not have to deliberate long in whether or not to allow you in.” The two Jedi began walking towards a clearing in the tree line ahead of them. “When your transport first landed on our home, we could feel a great presence in the force. We knew whoever arrived that day would benefit the galaxy greatly. Many masters were requesting to train you, but I was selected, and it has been a great honor to have done so.”

“Thank you Master.” Siddhartha replied. He reminisced on how hard of a decision it was to fly to Tython that day. When he left his home world of Lothal, he had left behind an honest life of work. When he left he knew that he was not just giving up his responsibilities to his family lineage, but that he was accepting the responsibility of the greater galaxy.

“Come now my apprentice, for us to commune to Naboo we shall seek approval from the Grand-master herself.” Quinn stated, as the two Jedi began to exit the training fields.

Siddhartha had only met the Grand-master of the Jedi once, on his initiation day into the Jedi Order. The thought of meeting her again filled Siddhartha with anxiety, as he even though he knew that she would approve of his assistance on Naboo, he worried that she would not approve due to his training having not been complete.

As Quinn and Siddhartha peaked the hill on the trail leading to the Jedi Temple on Tython, Siddhartha felt a warming grace upon viewing the temple. It was rebuilt from the ruins of the temple that once stood thousands of years prior, with a few updates to the structural integrity mandated by galactic codes. The architecture was beautiful, adorning statues of some of history’s greatest Jedi: Luke Skywalker, Obi Wan Kenobi, Yoda, and a few various other Jedi that Siddhartha did not recognize.

The two Jedi entered the temple, to find it booming with activity as usual. They made their way through the main hall to the meditation hall, and stopped to read the occupancy charts just outside the entrance.

“Hmmm...” Quinn hummed. “Grand-master Rey is usually in the Meditation Hall.. I wonder where she has gone.” Quinn stopped and thought to himself for a moment. Siddhartha stood by and observed his master’s thought process.

“I may not know where she is, but I do know who to ask.” Announced Master Quinn. With that statement he rushed off to another section of the temple, Siddhartha following close behind, wondering who his master was thinking of.

A large Wookiee placed the Jedi texts carefully on the library shelf where they belonged. Most of the sacred Jedi knowledge had been converted to either a holocron or data card form, but this particular Wookiee sought to personally care for the last remaining written texts. As he stepped down from his ladder to retrieve more recently returned data cards, the Wookiee is startled by Master Quinn and Siddhartha, almost trampling them over.

“Hello there!” Master Quinn exclaims.

The Wookiee lets out a pleasant roar in response. Siddhartha was a bit confused, as he had never spoken to a Wookiee before, and did not understand their language or native tongues.

“We are quite well.” Quinn replied. “We have come to see if you are aware of the whereabouts of Grand-master Rey, as she is not in the Grand Meditation Hall as she usually is.”

The Wookiee let out a large sigh, followed by a few dull moans as he explained to Master Quinn what he knew about Rey’s whereabouts.

“I see..” Master Quin replied, with his peaceful smile slowly fading. Siddhartha, feeling a smidgen embarrassed, placed his hand on Master Quinn’s should er to gain his attention.

“Master Quinn, I do not speak the tongues of the Wookiee, I don’t understand what he is saying.”

“Ah,” Quinn replied, “He was explaining that Grand-master Rey has entered a deep meditation in her personal quarters, as she believes that her time is drawing near to become one with the force.” Siddhartha’s expression turned to that of shock, as he was just as saddened as Master Quinn to hear the news.

“Oh where are my manners!” Master Quinn exclaimed. “You two have not met.” He placed his arm around Siddhartha and extended a palm to the large Wookiee. “This is my apprentice, Siddhartha. Siddhartha, this is Jedi Master Gungi. He is one of the Grand-masters of the Jedi Order. He was a padawan in the time of the Old Republic, and survived the great Jedi purge at the hands of Darth Sideous.”

Master Gungi nodded and lent a pleasant greeting to Siddhartha. Siddhartha shook the Wookiee’s hand, absolutely stunned at being in the presence of a Jedi Legend.

“You’re Master Gungi...” Siddhartha said, star struck. “I’ve read about you in the Jedi archives. You survived Order 66 and hid with you family on your home world of Kashyyk, and came back to help Grand-master Rey rebuild The Jedi Order.” Gingi nodded his head, and groaned as if to say “Indeed I am.”

“That’s right.” Master Quinn added. “He was the Jedi who helped reshape us into the order we are today.” The three Jedi began to walk along the endless data shelves that lined the Temple Library. Siddhartha still in awe of the stature and presence of Master Gungi.

“We were hoping to converse with Grand- master Rey about a request I had received from Naboo.” Master Quinn explained to Gungi. “The Minister of Natural Preservation wishes to utilize our services to restore the Gungan Forest that was lost in wars long ago. The Minister has even requested My apprentice specifically.”

Gungi groaned in interest. Since Siddhartha was still very young, he was not yet allowed to leave Tython without the supervision of his master, and approval from the Grand-master of the order herself. Since Rey was on a meditative leave, that left Gungi as the acting head Grand- master. He moaned on for a few more seconds, before delivering a decisive answer to Master Quinn. Siddhartha could tell what Master Gungi’s decision was by the immediate widening of Master Quinn’s smile.

“Wonderful!” Master Quinn exclaimed. “Siddhartha, Master Gungi has approved of you accompanying me on this adventure.” Siddhartha felt a warmth in his stomach that quickly filled up the rest of his body. “You will need to prepare quickly, as we will be leaving in the morning.”

“Yes Master, I will be awaiting for our departure in the Grand Hall first thing in the morning.” Siddhartha replied, then turning to Master Gungi, “Thank you Master Gungi, it was an honor to meet you.” He then turned and raced to his quarters. The sheer amount of excitement he felt running though his veins was exhilarating. He hadn’t been this excited since being informed that he was accepted into the Jedi Order.

On the morning that Master Quin and Siddhartha were set to leave, Siddhartha felt a strange uneasiness. He was nervous, just as he always was in taking on larger assignments at the temple, but this was different. He could feel that leaving the temple was a significant event in his life, but he couldn’t place a certainty as to why. He broke his stare at the floor to look up and see Master Quinn approaching.

“Have you slept well my apprentice?” Master Quinn inquired. Siddhartha had not, as the nervousness kept him awake all throughout the Tyhon night cycle.

“I did not master.” Siddhartha replied.

“Well that’s a shame,” Master Quinn replied, a look of concern taking over his expression, “good thing the journey to Naboo is not a short one, you will have plenty of time catch up on your rest.” Siddhartha’s nervous look turned to that of relief. Master Quinn had always been an expert in making Siddhartha feel confident in his abilities and his decisions. He felt his uneasiness subside for the moment, and found the courage in himself to continue on with their adventure.

“You are right master. Let’s go make the galaxy a better place.” With those words Master Quinn’s confident smile returned. The two Jedi grabbed their luggage then made their way to the star ship hangar to exit on their journey.

As they walked the halls to the star ship port, Siddhartha once again basked in the glory of the Jedi Architecture. Not being in the presence of a beautiful building would sadden Siddhartha, but he knew his journey was only temporary.

As the Jedi made their way through the stars on their way to the world of Naboo Siddhartha was taken back by the vastness of the space between the two worlds. He had only traveled through space once, when he left his home on Lothol to migrate to Tython. In that journey he had no window port to see the vastness between.

When the space ship carrying Master Quinn and Siddhartha dropped out of Hyperspace, the terrible feeling in Siddhartha’s stomach from before they departed Tython had returned, only this time much worse. This time, it made Siddhartha feel cold. As if he was not just making a great error in coming, but as if he was making a great mistake for everyone he knew or cared about.

“Master Quinn?” Siddhartha spoke up. He felt that if anyone could ease his mind, it would be his master.

“What is it Siddartha?” Master Quinn replied.

Siddhartha hesitated to fetch the words that described his feelings. After a brief pause, he found the words to express his immense uneasiness, “I have a bad feeling about this.”

---

So that was Chapter 1 of Star Wars Echoes of the Beyond. Did you like it? Hate it? Either way please do reach out and let me know.