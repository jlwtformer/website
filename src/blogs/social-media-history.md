# My History With Social Media, Why I Left, And Why I Am Returning

_Thursday, September 16, 2021_

Social Media, for better or for worse, has become an integral part of the human race. In some way or another, if you want to participate in the modern world you need to have a presence on social media. Like everyone else, I too have a history with Facebook, Twitter, Instagram, Reddit, etc.

As a large advocate of digital security and privacy, I tend to see social media in a negative light. However, there are ways that it has helped in connecting with others who may have no other means of communication. For now let's just talk about my history with this corner of the world wide web.

## My Social Media History, And Why I Left

My adventures in the digital social space began all the way back in 2007, when I was in what is referred to in the US as Junior High School. Back then the hit place to meet up online was a website called MySpace. This was when MySpace was still owned by the relatively lesser known man named Tom, before he sold the company and became a professional photographer (that's how the tale goes, may need correction on the last part of what Tom is up to now). MySpace let you customize your online profiles with custom backgrounds, music, and a list of your favorite friends from the contacts you've made on the website.

MySpace was a fun site, but soon became over shadowed by the likes of Facebook. Yes, the infamous Facebook. At first I was hesitant to join Facebook, citing it's lack of customization of user profile pages back in the early days. But of course, I didn't want to be left out so I joined in with an account as well, while MySpace faded out into obscurity.

From there my use of social media was the same as most people's in the late 2000's/early 2010's. I joined Twitter, Reddit, and eventually Instagram. In 2015 was my first brush with real cyber security as my Instagram had been hijacked. It was at that point that I learned the truth of password strength, and how mine were always very weak.

Around 2016 something terrible happened. Someone who I had never met had decided to use the public information that could be found on my Facebook account to stalk and harass myself and my wife. This person, who shall not be named, would use pictures of my child as their own, and would go on to steal one of our identities. I use this story to boost my stance on digital privacy, seeing as how neither Facebook nor local authorities at the time didn't seem to be able to do much to stop the harassment.

It was at that time that I had deleted my original Facebook. At the time I had leaned on it to communicate with most of my friends outside of college, so doing that was a big hit socially. I would eventually rejoin under a pseudonym account to attempt to remain in touch with those I had lost contact with. However, that didn't scratch my itch to be social. The social media giants have evolved since their birth, and turned into advertising platforms that catered more to investors than the users. Thus, after some time I decided to leave all social media all together, and would reach out to my friends directly instead.

## My Return To Social Media

So with my social life no longer being tied to the social media giants, why would I want to go back at all? Well, it turns out with most of modern society's dependency on social media, the best place to advertise is on said platforms.

What would I have to advertise? This blog of course! Even though I took the time to set up an [RSS feed for this site](https://jwestall.com/blogs/making-rss-feed.html), it is still a good idea to publish updates to different mediums, as a way to reach more viewers.

With that said, which social media site did I chose to advertise to? Well, most of them now a days are pretty crummy in regards to user privacy. The platform that made the most sense to me is Twitter. It operates as a form of RSS feed, and is one that I can use to reach more in the tech/hobby sphere.

So if you have a Twitter, go ahead and follow me [@jwestall_com](https://twitter.com/jwestall_com)! I do plan on posting all blog updates to twitter, as well as on my RSS feed. Thank you for reading my blog, and don't forget to reach out to me however you want to let me know your thoughts on social media, this blog, and anything else.
