# Creating a Chat Room For Readers of This Blog

_Thursday, September 2, 2021_

Well, August was quite the eventful moth for this blog, wasn't it? Jokes aside, I did have to put some hobbies to the side for now to catch up on work and other private happenings. But in the mean time, I wanted to find a more direct way for the readers of my blog to keep in touch, as email isn't always the best way.

To do that, I have decided to create a chat room on [Element](https://element.io/)! Now, why did I chose element over a more popular solution like Discord? Well, really for one reason: end to end encryption. Discord by default is not end to end encrypted, and as a support of the E2E standard I wanted to use a service that did have it by default.

Want to be a part of the chat room? Just [drop me an email](/cont.html) letting me know your interest and I will send you the link to join!

I am still working on a set of sensible rules for the chat room, but for now it will follow the words of Bill and Ted: be excellent to one another!
