# No, Your Computer Isn't Yours. But That Can Be Okay

_Tuesday, July 13, 2021_

So not long after the release of macOS Big Sur, security researcher Jeffrey Paul put out a blog post titled simply, [Your Computer Isn't Yours](https://sneak.berlin/20201112/your-computer-isnt-yours/). In this article he outlines how macOS 11 Big Sur was sending un-encrypted hashes of your usage data to Apple Servers every time you opened an app. He used this discovery along with other points of user hostile decisions by Apple in the past to come to the conclusion that the computer you probably spent a couple thousand on is not in fact your property.

I don't particularly agree with Jeffrey on his claims, at least not entirely. When it comes to hardware, you spent your money, and that hardware is now your property. Simple as that. The software aspect is where Jeffrey's catchy headline starts to take a ground in reality. Jeffrey in his blog post is making the claim that Apple spying on your data and not being able to be operated fully offline is how the computer ceases to be owned by you. To me, that doesn't make sense. As of macOS 11.2 this discovery has been patched, so that the hashes are encrypted, and so that it can be blocked or routed through a VPN.

But again, that doesn't mean that your computer is now yours once you update to macOS 11.2. As soon as you install macOS, or boot your Mac computer for the first time, you are agreeing to the macOS Terms of Service. Yes, that thing no one actually reads but still agrees to. Let's read the first part of that TOS:

```
1. General.
A. The Apple software (including Boot ROM code), any third party software, documentation, interfaces, content, fonts and any data accompanying this License whether preinstalled on Apple-branded hardware, on disk, in read only memory, on any other media or in any other form (collectively the “Apple Software”) are licensed, not sold, to you by Apple Inc. (“Apple”) for use only under the terms of this License. Apple and/or Apple’s licensors retain ownership of the Apple Software itself and reserve all rights not expressly granted to you. You agree that the terms of this License will apply to any Apple-branded application software product that may be preinstalled on your Apple-branded hardware, unless such product is accompanied by a separate license, in which case you agree that the terms of that license will govern your use of that product.
```

Now I imagine no one read that as it seems everyone is allergic to terms of service, so I'll highlight the important part.

**_(collectively the “Apple Software”) are licensed, not sold, to you by Apple Inc._**

**_Apple and/or Apple’s licensors retain ownership of the Apple Software itself and reserve all rights not expressly granted to you._**

That's right, macOS is _never_ given out to any users. Many people will find this clause to be completely justified, however it is a serious flaw in our digital tool set if we don't explicitly own the tools we are using.

But let's try to put a positive spin on this. In the context of macOS, Apple retaining ownership of their operating system _could_ be beneficial for a few reasons:

* Security: Since Apple owns the OS, it is their responsibility to keep the system up to date and ahead of the cyber security curve. Granted if you read all of the macOS TOS I'm sure they will still state that you are responsible with how you use the OS, which is also true. A warranty on a window is no good if you intentionally throw a rock through it.
* Compatibility: One of Apple's strong suits is it's ability to make all of the first party accessories work together, and the "ecosystem" of Apple devices will always just work, thanks to them taking responsibility for the ownership of the OS.
* Privacy: This one piggy backs off of the Security point, but since Apple owns the OS, it's up to them to make it as privacy friendly as possible. Whether or not you can actually trust Apple to do that is another discussion, which we are about to get to.

But letting Apple own macOS, and even letting Microsoft own Windows (yes the Windows terms of service say similar things as the macOS one) isn't all sunshine and roses. Apple has had a sketchy track history when it comes to user privacy and security, even bowing to NSA data requests as recently as this year. When it comes to storing sensitive information about yourself, trusting a for profit company to store it safely is simply a bad idea.

That's where the idea of compartmentalization comes into play. If you find yourself in a situation where you need, or simply just want to use (like me), macOS or Windows then it is a good idea to compartmentalize your data and computer use. Let macOS or Windows be the tool you use for non sensitive work, like making 3D print models, writing a blog, or creating an open source app. But for all the sensitive tasks, say storage of all your photos or tax documents, try to keep them on a separate hard drive that isn't plugged into the computer 24/7. (I do plan on discussing data storage on this blog soon). Sensitive work should be done on alternate operating systems, like Linux or OpenBSD, as they are known for not sending usage data to companies without user knowledge.

At the end of the day I do still like using macOS. The user interface is the best I've ever used, and the compatibility with first party software while maintaining a UNIX like backend is not to be overlooked. But Jeffrey Paul was right, when you own a Mac computer, or if you run Windows, your computer is not to be considered yours.