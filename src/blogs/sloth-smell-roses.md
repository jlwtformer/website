# Sloth and Smell the Roses

_Tuesday, June 1, 2021_

My name is Jacob. I am on the better half of my 20's, getting pretty close to my 30's. I have done plenty of things in my life. I have played drums in front of packed arenas, moved pipe on oil rigs, worked in tech support for the largest tech company there is, fathered a handful of kids, landscaped for a large University, written for tech blogs, designed official concepts for University Lab Structures, designed drone class planes, developed a tool to automate my job, and even done engineering for government contractors. Even with all my accomplishments and achievements, I still struggle to feel like I have found my lot in life. Aside from my family, I still feel like I want more out of life. When I am not with my better half and our lovely children, I always feel as if I should be doing more, and find myself wanting more and more.

That was until I had a revelation when reading a book with my oldest child. They had gotten a book recently called "Sloth and Smell the Roses". It's a book that teaches the concept of mindfulness to children, a concept that has deep roots in Buddhism. If you didn't know, at one point in high school my nickname was "Buddha". The nickname was more of a joke among my peers, as when I would get a buzz cut, my hair was soft enough that if you pet my head it would tickle your hand and make you laugh. My friends equated this to rubbing the Buddha's belly and gave me the nickname.

![Cover for Sloth and Smell the Roses](/res/sloth-cover.jpg)

After carrying the nickname for some time I began to actually look into Buddhism, and discovered the four noble truths. Boiled down, they essentially say the following:

* All life, and all beings will experience suffering
* We all deal with suffering because we hold onto attachments
* There is a way to escape the suffering, by escaping our attachments
* We can escape our attachments by following the Noble Eightfold Path (aka: living a modest life)

Having my oldest child's book remind me of these teachings helped me to realize why I am struggling so much to find my footing in the world, and what my root problem is in finding a place for myself. My big issue is that I am wanting to find a place, not that I need one. Because at the end of the day, I already have found my place.

As I mentioned before I have done a lot of things, and have picked up a lot of hobbies along the way. I enjoy sharing those hobbies, and as such kept thinking of ways to share what I wanted more and more, to gain a larger audience around me. What I was missing is that the best audience I could ever ask for was already around me, in the friends and family I had made along the way. So when it comes to my hobbies, I am seeking to stop seeking. I won't try to find ways to add to a hobby or to share it in the most influencer way possible, rather I'll just experience them as they come. I'll do my best to find joy in what is around me, including my family.

But that doesn't mean that I will want to stop sharing them. I do enjoy sharing, and will continue to do so, but in a less involved way. Instead of trying to make a career out of a niche YouTube channel, I'll just blog out whatever is interesting to me, and if people like to read about it hey that's awesome.

So if you want to keep up with what hobbies I am into or whatever things I find interesting, keep following this blog. I do plan on looking into implementing an RSS feeder for this site, so you can integrate it into your media intake however you please. Don't hesitate to reach out to suggest things you might find interesting and want my take on. I find hobbies and interests through the experience of others as well.

And one last thing, finding the resources for some of my hobbies does come at a financial cost, so if you want to see more you are encouraged (but not required!) to donate, if you feel like you want to go the extra mile in helping out. Note that there is no incentive to donating, other than providing me with a budget to blow on hobby materials. Donation link coming soon. 

Thank you for reading, I hope you come back soon.