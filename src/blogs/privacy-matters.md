# Privacy Isn't About What You Have To Hide, It's About What You Have To Lose

_Sunday, July 25, 2021_

If you aren't aware, a pretty nasty spyware was discovered recently. The spyware, called [Pegasus](https://www.theguardian.com/news/2021/jul/18/what-is-pegasus-spyware-and-how-does-it-hack-phones) would allow a Group Called the NSO to record your calls, copy your messages and secretly film you. It's been making waves throughout the privacy and security landscape, but one particular aspect of this has really hit me the wrong way. NSO Group CEO, who's name I don't care about honestly, stated that [law-abiding citizens have 'nothing to be afraid of'](https://appleinsider.com/articles/21/07/23/nso-group-ceo-says-law-abiding-citizens-have-nothing-to-be-afraid-of).

And quite frankly that is a load of bull crap. If we have nothing worth finding, why are you looking in the first place? The 'nothing to hide' argument has been very common amongst these debates, and has often been touted from the parties who are doing most of the unnecessary snooping.

![Pretty much.](/res/funny-reddit-comment.png)

But the issue is that caring about your security and privacy isn't about what you have to hide, it's about what you have to lose. Not sure what that means? Well it's simple. We'll take the age old security analogy of leaving your front door unlocked at night. If you lived in a sketchy neighborhood, you'd lock your doors right? Why? Because you are wanting to hide your couch right? No. You lock it so a bunch of kids don't walk in your house and steal your flat screen tv.

And that is why digital security and privacy is so important to so many people. We are not hiding things to avoid getting caught doing something nefarious, we're trying to protect ourselves. In my personal threat model, I have to worry about blood relatives who will use my personal information, and my family's personal information, to harm me and my family. No, you knowing my address or personal phone number may not be that bad, but these people I am trying to protect myself from would use that information to harass and harm my family.

So I will continue to care, and in my opinion you should too. Because if more people care about their security and privacy, and more people actually do something to protect themselves from these nefarious actors like NSO and company, the easier it will be for those like me who are just trying to protect those around them. Thanks for coming to my TED Talk.