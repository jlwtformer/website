# A Wild Donation Link Appears!

_Wednesday, June 17, 2021_

So as I had promised back in my [first ever blog post](/blogs/sloth-smell-roses.html), I have set up a donation link so that those of you who feel the need to support me monetarily may now do so! The donation platform I went with was LibrePay, a free software based donation platform, that is very similar to Patreon. The platform is set up to take re-occurring monthly donations, however I want to stress that that is optional. If you only want to donate a single time I do have a PayPal account that you can send a one time donation to (which can be found on my LibrePay profile).

Something I said in my first post that I want to reiterate here is that donations will not be mandatory to keep my blog going, and will not be needed to view the blog. It is something that I decided to include as having hobbies that include building props, building furniture, and doing research on a wide variety of topics takes time and money. If this blog is to have a lot of that content, it would be nice to have some sort of funding from those that find it interesting to keep it going. Once again, you are not required to donate to me. I am simply giving the donation link as an option, in case you feel strongly that you want to. Thank you for reading my blog and supporting me.

Donate today at [LibrePay](https://liberapay.com/Jacob_Westall/)!