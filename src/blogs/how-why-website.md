# How and Why I Run This Website

_Wednesday, June 30, 2021_

So for a long time the jwestall.com domain existed as a "live" copy of my resume so that I could keep control on what potential employers saw of my experience and background. I had run into the issue of recruiters modifying my resume without my knowledge and pretending I had experience where I did not. As you can imagine, this did not look good when I did land job interviews and the interviewer would be asking me in depth questions about tool sets I did not know, only for them to angrily ask why I put that I knew it on my resume.

And with that painful experience behind me began the process of making a central, public copy of my resume. The website was written in pure HTML/CSS by hand, and hosted at my house through a Raspberry Pi server. That server was nice, and eventually became the hosing server for my personal cloud when I began to step away from Google services.

But eventually that server died when the RPi's SD Card got corrupted and refused to boot. From there I moved my site to its current host, Gitlab pages. From there I moved the creation and addition to this site to a simple format, being written in markdown and compiled to the static pages you are reading now. If you're a little lost with all the website terms don't worry, it'll get simple in a minute.

Eventually like many others, I began to fall out of love with the concept of social media. The platforms available began to feel less like places to digitally gather and socialize, and began to feel more like digital ad markets where I am not a visitor but rather a product. To contrast that to the real world, it's like my favorite bar getting bought out and removing all the good beer and replacing it with cheap crap. Wanting to continue socializing online, I began turning my website into the blog you are currently reading. I like hosting my own blog as it feels like having a house party where I can serve whatever beer I want to. If you are interested in hosting your own website (I suggest you do!), then please keep reading.

Before getting into the specifics of how this website is built, it's important to know the difference between static and dynamic websites. The big difference is how the content is served, meaning when you refresh the webpage what will come up? A static site is served as is, and will not change when the page is refreshed. Dynamic pages are served based on a changing set of content, meaning a page refresh will result in a different page loading. Gitlab pages currently only supports static web pages, which given the free price of hosting makes sense.

## Getting Our Web Tools Setup

So I use a few tools to write this site/blog, and with them come some requirements to use. To write the content, I write the article documents in markdown. [Markdown](https://guides.github.com/features/mastering-markdown/) is a very simple markdown syntax that is super easy to learn and get going. While I mainly write in [Visual Studio Code](https://code.visualstudio.com/), you can use just about any text editor to write markdown files, just as long as you can save the file with the `.md` file format.

To "compile" these markdown files into static web pages, I use two tools that work together, [ssg6](https://www.romanzolotarev.com/ssg.html) and [lowdown](https://kristaps.bsd.lv/lowdown/). This is where the operating requirements come into play. As it stands both ssg6 and lowdown can only run on Unix based kernels, which really just means it will not work on Windows. If you do run Windows, you might find some luck running them in [WSL](https://docs.microsoft.com/en-us/windows/wsl/). I have gotten these tools to work on both Linux and macOS, so if you use those operating systems then you should be fine.

We are going to want to setup two git repositories as well, one [regular repository](https://docs.gitlab.com/ee/user/project/working_with_projects.html#create-a-project), and one [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) (or [Github Pages](https://pages.github.com/)) repositories. We will use the regular repo to store our in progress documents, and the Git Pages repo to store the finalized and published copy of our website. While there will be an 'output' folder in our in progress repo, it's smart to keep the public copy separated just so you can view any issues or errors before publishing the content publicly.

## Writing and Posting Our Website

Writing the content of the site is not very hard. As stated before, the bulk of the content is written in markdown format. ssg6 will require that you create a `_header.html` and `_footer.html` document to store the common html elements that will show in all pages of your website. If you want a base of reference on what you can include, you can check out my [website repository](https://gitlab.com/jlwtformer/website/-/tree/master/src) to see how the 'source' side is set up.

After you have written your content, you can use ssg6 to 'compile' your files into your output folder. Then, copy all the new and modified files over to the Git Pages repo, and sync it all up to publish.

This simple setup works really well for me, as it is a way to write and contribute to my blog quick and efficiently. If you do want to start your own blog, and decide to follow how I make mine please reach out and share your results as well! I will always be happy to help troubleshoot any issues you run into as well.

If you feel that this guide needs revised, please do submit a pull request on my [website's repository](https://gitlab.com/jlwtformer/website/-/blob/master/src/how-why-website.md) so that the information can remain useful for as long as possible.