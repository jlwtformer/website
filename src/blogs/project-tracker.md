# Project Tracker

A somewhat up to date project tracker document for my vlog projects. May be some blog projects as well. Feel free to reach out via email, Mastodon, or Matrix to discuss.

## GTK Rust tutorial part 2

Will cover subclassing and maybe composite templates.

* ~~Research~~
* ~~Planning~~
* Recording
* Initial edit
* Reshoot recording
* Secondary edit
* Polish
* Review
* Final polish
* Publish

## Drumming Short

Short vid about practicing drums in the suburbs.

* Planning
* Recording
* Edit
* Polish
* Publish

## Best uses for a 3D printer.

Showing a couple of the best use I've gotten from my 3D printer.

* ~~Planning~~
* Gather 3D files
* Slice to GCode
* Print parts
* Assemble parts
* Recording
* Initial edit
* Reshoot recording
* Secondary edit
* Polish
* Review
* Final polish
* Publish

## 3D Printed Mando Armor

Creating the top half of a 3D printed Mandalorian armor set.

* ~~Conceptualize~~
* ~~Gather 3D files~~
* ~~Slice to GCode~~
* Print parts
* Assemble parts/paint
* Recording
* Initial Edit
* Reshoot recording
* Secondary edit
* Polish
* Review
* Final polish
* Publish

## GTK Rust tutorial part 3

Coming after part 2. More than likely will cover packaging and distribution.

* Research
* Planning
* Recording
* Initial edit
* Reshoot recording
* Secondary edit
* Polish
* Review
* Final polish
* Publish

---

## Writer's Block

Short Story about a dude. With writer's block.

* Planning/Story Boarding
* First Draft
* Initial edit
* Second Draft
* Secondary edit
* Polish
* Review
* Final polish
* Publish

## The Importance Of Self Hosting

Explination as to why one should self host services if they are able to.

* Research
* Planning
* First Draft
* Initial edit
* Second Draft
* Secondary edit
* Polish
* Review
* Final polish
* Publish
