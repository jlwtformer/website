# Contact

Below you will find my contact information. The best way to reach out is by email, as it is checked more often. I highly suggest using GnuPG to encrypt your message, so we may communicate in a private and secure manner.

Jacob Westall\
[jacob@jwestall.com](mailto:jacob@jwestall.com?subject=New%20Web%20Inquiry)

Public Fingerprint: `60FC 6176 612A 1476 3045  BAA9 6894 8833 CC1F 3677`

Import my Public Key: `curl https://jwestall.com/res/jacobwestall.asc | gpg --import`

## Social Media

I do infact exist on social media in a limited capacity. You can follow me on [Twitter](https://twitter.com/jwestall_com) if you would like to keep up to date with my blog.
