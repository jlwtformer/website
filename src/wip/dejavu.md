# Short Story: Déjà-Vu

Skeleton:

* Richard is walking down the street. He comes up on a couple breaking up. He starts to get a massive headache and notices what looks like a small black hole open in front of him, he falls into it.
* When he wakes up he's in some space rock, with rivers of light flowing around his feet. He gets up and looks around, seeing a person raking the rocks around, controlling the flowing light.
* Rich approaches the person and plays 20Q. The person is called The Janitor, and is keeping an eye on the flow of time.
* Rich came across a branching event in time, and just happened to slip through the cracks, coming out at the Janitor's realm.
* Rich asks how to get home, simple as jumping into any stream of light he wants. "Congrats you discovered time travel"
* Rich asks how the branches are managed, what happens if/when they come back together. The Janitor explains that the feeling of Déjà-Vu is what actually happens when two flows of time converge.
* Rich asks about how The Janitor came to be, he responds by saying he was like Rich. He fell out of the time stream and just decided to stick around to keep the flow of time from developing into a WW3 or anything too sinister.
* Rich decides to return, and jumps in right where he fell from.
* He then wakes on the ground with a group of people surrounding to make sure he's okay.

---

Richard walked quietly down the street, hands in his hoodie pocket. The light rain patted his back gently, reminding him of his consciousness. He didn't anticipate the weather, letting the puddles on the sidewalk soak through his shoes into his socks. He was cold, but still warm with emotions.

_I can't believe she would do that to me._ He thought to himself. _After all this time, after all we've been through._

As the rain continued to pour down on Richard, he tried his best to forget the horrible truth he had just learned. Walking down the store fronts, flashes of past dates came through his mind. Luckily for him, a rather heated argument up the way was able to break his chain of thought.

"How in the hell did you think this was okay?!" A young woman shouted at a man standing her opposite.

The words cut through the sound of the rain and grabbed Rich's attention. The man on her opposite tried to speak up to defend himself.

"Look Jan I-"

"No. Just don't." Jan snapped back at him as soon as he started to talk.

The conversation seemed to be a direct mirror of the one Rich had just had earlier that day. He could tell what happened. Seeing the parallels unfold in front of him aroused a strange feeling in the back of his mind. He began to feel a mix of nausea mixed with a headache that split through his head down the middle. Before he knew it, Rich was falling onto the street and losing conscience. As the dark black continued to flood his vision he noticed Jan and her male friend running in attempt to catch him.

---

As Rich came to the feeling in his head was gone. He was no longer in the street being coated by rain, but rather laying face down in a clearing of fine black gravel. He pulled himself up to dust off, and was amazed at the sight around him.

Splitting the gravel he sat on were rivers of what appeared to be pure light. Richard was awe-struck at the sight, seeing how the rivers branched and intertwined, looking like the footprint of grand tree. Across the landing he spotted a 