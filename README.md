# Jacob Westall's Personal Website

This website is a simple portal to my technical background and experience. It is written in markdown files and compiled with [ssg6](https://www.romanzolotarev.com/ssg.html) and [lowdown](https://kristaps.bsd.lv/lowdown/).